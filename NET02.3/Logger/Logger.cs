﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using ListenerInterface;

namespace Logger
{
    public class Logger
    {
        private readonly List<IListener> _listeners = new List<IListener>();

        public Logger(string appName)
        {
            Configuration cfg = ConfigurationManager.OpenExeConfiguration(appName);
            CustomApplicationConfigSection config = (CustomApplicationConfigSection)cfg.GetSection("CustomApplicationConfig");

            foreach (ListenerConfigElement listenerConfig in config.Listeners)
            {
                Console.WriteLine(listenerConfig.Type);
                AssemblyName assemblyName = new AssemblyName(listenerConfig.Type);

                try
                {
                    Assembly assembly = Assembly.Load(assemblyName);
                    Type type = assembly.GetType(listenerConfig.Type + "." + listenerConfig.Type);
                    IListener listenerObject = (IListener)Activator.CreateInstance(type);
                    if (listenerConfig.Params != null)
                    {
                        listenerObject.Init(listenerConfig.Params.Cast<object>().ToList());
                    }
                        //if (listenerConfig.Params != null)
                        //{
                        //    PropertyInfo[] properties = listenerObject.GetType().GetProperties();
                        //    foreach (PropertyInfo property in properties)
                        //    {
                        //        MethodInfo mi = property.SetMethod;
                        //        foreach (ParamConfigElement parameter in listenerConfig.Params)
                        //        {
                        //            if (parameter.Property == property.Name)
                        //            {
                        //                mi.Invoke(listenerObject, new object[]
                        //                {
                        //                    parameter.Value
                        //                });
                        //            }
                        //        }
                        //    }
                        //}
                    _listeners.Add(listenerObject);
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine(listenerConfig.Type + ".dll was not found");
                }
            }
        }

        public void Warning(string message)
        {
            WriteToLog("[WARNING]:" + DateTime.Now + " - " + message);
        }

        public void Error(string message)
        {
            WriteToLog("[ERROR]:" + DateTime.Now + " - " + message);
        }

        public void Message(string message)
        {
            WriteToLog("[MESSAGE]:" + DateTime.Now + " - " + message);
        }

        private void WriteToLog(string s)
        {
            if (_listeners == null)
            {
                throw new NullReferenceException("No listeners added!");
            }

            foreach (IListener listener in _listeners)
            {
                listener.Write(s);
            }
        }

        public void Track(object obj)
        {
            Type t = obj.GetType();
            Console.WriteLine(" + " + t);
            Attribute entityAttribute = Attribute.GetCustomAttribute(t, typeof(TrackingEntityAttribute));

            if (entityAttribute != null)
            {
                FieldInfo[] fields = t.GetFields();
                foreach (FieldInfo field in fields)
                {
                    Attribute fieldAttribute = Attribute.GetCustomAttribute(field, typeof(TrackingPropertyAttribute));

                    if (fieldAttribute != null)
                    {
                        if (!string.IsNullOrEmpty(((TrackingPropertyAttribute)fieldAttribute).Name))
                        {
                            WriteToLog("«" + ((TrackingPropertyAttribute)fieldAttribute).Name + "» = «" + field.GetValue(obj) + "»");
                        }
                        else
                        {
                            WriteToLog("«" + field.Name + "» = «" + field.GetValue(obj) + "»");
                        }
                    }
                }
                PropertyInfo[] properties = t.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    Attribute propertyAttribute = Attribute.GetCustomAttribute(property, typeof(TrackingPropertyAttribute));
                    if (propertyAttribute != null)
                    {
                        if (!string.IsNullOrEmpty(((TrackingPropertyAttribute)propertyAttribute).Name))
                        {
                            WriteToLog("«" + ((TrackingPropertyAttribute)propertyAttribute).Name + "» = «" +
                                       property.GetValue(obj) + "»");
                        }
                        else
                        {
                            WriteToLog("«" + property.Name + "» = «" + property.GetValue(obj) + "»");
                        }
                    }
                }
            }
        }
    }
}

