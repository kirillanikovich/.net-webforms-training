﻿using System;

namespace Logger
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; }

        public TrackingPropertyAttribute()
        {
            Name = null;
        }

        public TrackingPropertyAttribute(string name)
        {
            Name = name;
        }
    }
}