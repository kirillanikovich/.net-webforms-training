﻿using System;

namespace Logger
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
    public class TrackingEntityAttribute : Attribute
    {
        public TrackingEntityAttribute()
        {
        }
    }
}