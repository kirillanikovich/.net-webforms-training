﻿using System.Configuration;

namespace Logger
{
    public class ListenerConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("Type")]
        public string Type
        {
            get
            {
                return base["Type"] as string;
            }
        }

        [ConfigurationProperty("Params")]
        public ParamConfigCollection Params
        {
            get { return base["Params"] as ParamConfigCollection; }
        }
    }
}