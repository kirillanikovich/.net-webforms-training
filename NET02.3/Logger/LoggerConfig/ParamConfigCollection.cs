﻿using System.Configuration;

namespace Logger
{
    [ConfigurationCollection(typeof(ParamConfigElement), AddItemName = "Param")]
    public class ParamConfigCollection : ConfigurationElementCollection
    {
        public ParamConfigElement this[int index]
        {
            get { return (ParamConfigElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ParamConfigElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ParamConfigElement)element).Property;
        }
    }
}