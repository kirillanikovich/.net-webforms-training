﻿using System.Configuration;

namespace Logger
{
    public class ParamConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("Property")]
        public string Property
        {
            get { return base["Property"] as string; }
        }

        [ConfigurationProperty("Value")]
        public string Value
        {
            get
            {
                return base["Value"] as string;
            }
        }
    }

}