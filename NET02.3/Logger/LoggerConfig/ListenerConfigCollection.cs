﻿using System.Configuration;

namespace Logger
{
    [ConfigurationCollection(typeof(ListenerConfigElement), AddItemName = "Listener")]
    public class ListenerConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerConfigElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ListenerConfigElement)element).Type;
        }
    }
}