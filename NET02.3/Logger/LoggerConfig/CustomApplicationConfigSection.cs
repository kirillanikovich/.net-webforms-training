﻿using System.Configuration;


namespace Logger
{
    public class CustomApplicationConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Listeners")]
        public ListenerConfigCollection Listeners
        {
            get { return base["Listeners"] as ListenerConfigCollection; }
        }
    }
}