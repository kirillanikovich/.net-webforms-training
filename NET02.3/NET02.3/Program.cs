﻿using System;
using NET02._3.AttributeTestClasses;

namespace NET02._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Logger myLogger = new Logger.Logger("NET02.3.exe");
            myLogger.Message("Write the message test");
            myLogger.Warning("Write the WARNING test");
            myLogger.Error("Write the E R R O R test");
            myLogger.Track(new AttributeTestClass1() { StringField1 = "sf1", StringProperty1 = "sp1"});
            myLogger.Track(new AttributeTestClass2() { StringField2 = "sf3" });
            myLogger.Track(new AttributeTestClass2() { StringProperty2 = "sp3" });
            Console.Read();
        }
    }
}
