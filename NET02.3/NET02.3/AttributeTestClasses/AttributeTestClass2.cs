﻿using Logger;

namespace NET02._3.AttributeTestClasses
{
    [TrackingEntity]
    public class AttributeTestClass2
    {
        [TrackingProperty("TP.Name String Field 3")]
        public string StringField2;

        public string Stringfield21;
        [TrackingProperty]
        public string StringProperty2 { get; set; }
        
    }
}