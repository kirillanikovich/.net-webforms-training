﻿using ListenerInterface;
using Logger;
using System;
using System.Collections.Generic;
using System.IO;

namespace TextListener
{
    public class TextListener : IListener
    {
        public string Directory { get; set; }
        public string Filename { get; set; }
        
        public TextListener()
        {
            Directory = Environment.CurrentDirectory + "\\DefaultTXTlog\\";
            Filename = "log.txt";
        }

        private void CreateLog()
        {
            if (!System.IO.Directory.Exists(Directory))
            {
                System.IO.Directory.CreateDirectory(Directory);
            }

            if (!File.Exists(Directory + Filename))
            {
                File.WriteAllText(Directory + Filename, "Log started: " + DateTime.Now + Environment.NewLine);
            }
        }

        public void Write(string message)
        {
            CreateLog();
            File.AppendAllText(Directory + Filename, (message + Environment.NewLine));
        }

        public void Init(List<object> list)
        {
            foreach (ParamConfigElement parameter in list)
            {
                switch (parameter.Property)
                {
                    case "Directory":
                        Directory = parameter.Value;
                        break;
                    case "Filename":
                        Filename = parameter.Value;
                        break;
                }
            }
        }
    }
}
