﻿using System.Collections.Generic;
using System.Diagnostics;
using ListenerInterface;
using Logger;

namespace EventLogListener
{
    public class EventLogListener : IListener
    {
        public string Source { get; set; }

        public void Init(List<object> list)
        {
            foreach (ParamConfigElement parameter in list)
                switch (parameter.Property)
                {
                    case "Source":
                        Source = parameter.Value;
                    break;
                }
        }

        public void Write(string message)
        {
            if (!EventLog.SourceExists(Source))
            {
                EventLog.CreateEventSource(Source, "MyNewLog");
                return;
            }
            EventLog myLog = new EventLog();
            myLog.Source = Source;
            myLog.WriteEntry(message);
        }
    }
}