﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListenerInterface;
using Logger;
using Xceed.Words.NET;

namespace WordListener
{
    public class WordListener : IListener
    {
        public string Directory { get; set; }

        public WordListener()
        {
            Directory = Environment.CurrentDirectory + "\\DefaultDOCXlog\\";
        }

        public void Write(string message)
        {
            string pathDocx = Directory + "log.docx";
            if (!System.IO.Directory.Exists(Directory))
            {
                System.IO.Directory.CreateDirectory(Directory);
            }
            if (!System.IO.Directory.GetFiles(Directory).Contains(Directory + "log.docx"))
            {
                DocX docCreate = DocX.Create(pathDocx);
                docCreate.InsertParagraph("File created." + DateTime.Now + Environment.NewLine);
                docCreate.Save();
            }
            DocX docLoad = DocX.Load(pathDocx);
            docLoad.InsertParagraph(message);
            docLoad.Save();
        }

        public void Init(List<object> list)
        {
            foreach (ParamConfigElement parameter in list)
            {
                switch (parameter.Property)
                {
                    case "Directory":
                        Directory = parameter.Value;
                        break;
                }
            }
        }
    }
}
