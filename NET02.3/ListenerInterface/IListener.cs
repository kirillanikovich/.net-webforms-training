﻿using System.Collections.Generic;

namespace ListenerInterface
{
    public interface IListener
    {
        void Write(string message);
        void Init(List<object> list);
    }
}
