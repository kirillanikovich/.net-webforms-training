﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MVCproject.BLL.Models;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.BLL.Interfaces
{
    public interface IPizzaService : IService
    { 
        PizzaDTO GetPizza(int? id);
        PizzaDTO GetPizzaByName(string name);

        IEnumerable<PizzaDTO> GetPizzas();
        void UpdatePizza(PizzaDTO pizza);
        void CreatePizza(PizzaDTO pizza);
        void DeletePizza(int id);
    }
}
