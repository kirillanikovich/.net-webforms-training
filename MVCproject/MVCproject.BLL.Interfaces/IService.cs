﻿using MVCproject.DAL.Interfaces;

namespace MVCproject.BLL.Interfaces
{
    public interface IService
    {
        IUnitOfWork Database { get; set; }
        void Dispose();
    }
}
