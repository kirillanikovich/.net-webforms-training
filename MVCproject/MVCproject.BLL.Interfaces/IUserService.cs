﻿using System.Collections.Generic;
using MVCproject.BLL.Models;

namespace MVCproject.BLL.Interfaces
{
    public interface IUserService: IService
    {
        UserDTO GetUser(int? id);
        UserDTO GetUser(string login);
        List<UserDTO> GetUsers();
        List<RoleDTO> GetRoles();
        void ChangeRoles(UserDTO user);
        void DeleteUser(int id);
        void CreateUser(UserDTO user);
        void UpdateUser(UserDTO model);
    }
}
