﻿using System.Collections.Generic;
using MVCproject.BLL.Models;
using MVCproject.BLL.Models.Cart;

namespace MVCproject.BLL.Interfaces
{
    public interface IOrderService : IService
    {
        void MakeOrder(IEnumerable<CartItem> cart, OrderDTO order);
        List<OrderDTO> GetOrders();
    }
}
