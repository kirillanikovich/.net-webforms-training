﻿using System.Collections.Generic;
using MVCproject.BLL.Models;

namespace MVCproject.BLL.Interfaces
{
    public interface IPizzaCategoryService : IService
    {
        PizzaCategoryDTO GetPizzaCategory(int? id);
        IEnumerable<PizzaCategoryDTO> GetPizzaCategories();
        void CreatePizzaCategory(PizzaCategoryDTO model);
        void UpdatePizzaCategories(ICollection<PizzaCategoryDTO> pizzaCategories);
        void DeletePizzaCategory(int id);
    }
}
