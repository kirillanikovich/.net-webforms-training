﻿using System.Collections.Generic;
using MVCproject.BLL.Models;

namespace MVCproject.BLL.Interfaces
{
    public interface IPizzaTypeService : IService
    {
        PizzaTypeDTO GetPizzaType(int? id);
        IEnumerable<PizzaTypeDTO> GetPizzaTypes();
        IEnumerable<PizzaTypeDTO> GetPizzaTypesByPizzaId(int pizzaid);
        void UpdatePizzaType(PizzaTypeDTO pizzaType);
        int CreatePizzaType(PizzaTypeDTO pizzaType);
        void DeletePizzaType(int id);
        string GetPizzaNameByPizzaTypeId(int? id);
    }
}
