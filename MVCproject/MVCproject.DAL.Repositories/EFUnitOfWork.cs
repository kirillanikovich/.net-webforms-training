﻿using System;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;

namespace MVCproject.DAL.Repositories
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private MVCprojectDBContext        _db;

        private IRepository<PizzaType>     _pizzatypeRepository;
        private IRepository<Pizza>         _pizzaRepository;
        private IRepository<PizzaCategory> _pizzacategoryRepository;
        private IRepository<User>          _userRepository;
        private IRepository<Role>          _roleRepository;
        private IRepository<Order>         _orderRepository;

        public EfUnitOfWork(string connectionString)
        {
            _db = new MVCprojectDBContext(connectionString);
        }

        public IRepository<PizzaType> PizzaTypes
        {
            get
            {
                if (_pizzatypeRepository == null)
                    if (_db != null)
                        _pizzatypeRepository = new GenericRepository<PizzaType>(_db);
                return _pizzatypeRepository;
            }
        }

        public IRepository<Pizza> Pizzas
        {
            get
            {
                if (_pizzaRepository == null)
                    _pizzaRepository = new GenericRepository<Pizza>(_db);
                return _pizzaRepository;
            }
        }

        public IRepository<PizzaCategory> PizzaCategories
        {
            get
            {
                if (_pizzacategoryRepository == null)
                    _pizzacategoryRepository = new GenericRepository<PizzaCategory> (_db);
                return _pizzacategoryRepository;
            }
        }

        public IRepository<Role> Roles
        {
            get
            {
                if (_roleRepository == null)
                    _roleRepository = new GenericRepository<Role>(_db);
                return _roleRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new GenericRepository<User>(_db);
                return _userRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new GenericRepository<Order>(_db);
                return _orderRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
