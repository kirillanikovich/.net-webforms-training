using System.Data.Entity;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Repositories.Mapping;
using MVCproject.DAL.Repositories.Models.Mapping;

namespace MVCproject.DAL.Repositories
{
    public class MVCprojectDBContext : DbContext
    {
        static MVCprojectDBContext()
        {
            Database.SetInitializer<MVCprojectDBContext>(null);
        }

        public MVCprojectDBContext(string connectionString)
            : base(connectionString)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<PizzaCategory> PizzaCategories { get; set; }
        public DbSet<PizzaType> PizzaTypes { get; set; }
        public DbSet<PizzatypeOrderXref> PizzatypeOrderXrefs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new PizzaMap());
            modelBuilder.Configurations.Add(new PizzaCategoryMap());
            modelBuilder.Configurations.Add(new PizzaTypeMap());
            modelBuilder.Configurations.Add(new PizzatypeOrderXrefMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());
        }
    }
}
