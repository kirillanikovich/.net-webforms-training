using System.Data.Entity.ModelConfiguration;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Repositories.Mapping
{
    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            // Primary Key
            HasKey(t => t.OrderId);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(255);

            Property(t => t.Adress)
                .IsRequired()
                .HasMaxLength(255);

            Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            ToTable("Order");
            Property(t => t.OrderId).HasColumnName("OrderId");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Adress).HasColumnName("Adress");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Date).HasColumnName("Date");
            Property(t => t.UserId).HasColumnName("UserId");

            // Relationships
            HasRequired(t => t.User)
                .WithMany(t => t.Orders)
                .HasForeignKey(d => d.UserId);

        }
    }
}
