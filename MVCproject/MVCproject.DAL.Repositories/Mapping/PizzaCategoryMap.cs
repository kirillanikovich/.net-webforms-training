using System.Data.Entity.ModelConfiguration;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Repositories.Mapping
{
    public class PizzaCategoryMap : EntityTypeConfiguration<PizzaCategory>
    {
        public PizzaCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.CategoryId);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("PizzaCategory");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
