using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Repositories.Mapping
{
    public class PizzatypeOrderXrefMap : EntityTypeConfiguration<PizzatypeOrderXref>
    {
        public PizzatypeOrderXrefMap()
        {
            // Primary Key
            this.HasKey(t => new { t.PizzatypeId, t.OrderId });

            // Properties
            this.Property(t => t.PizzatypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OrderId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PizzatypeOrderXref");
            this.Property(t => t.PizzatypeId).HasColumnName("PizzatypeId");
            this.Property(t => t.OrderId).HasColumnName("OrderId");
            this.Property(t => t.Count).HasColumnName("Count");

            // Relationships
            this.HasRequired(t => t.Order)
                .WithMany(t => t.PizzatypeOrderXrefs)
                .HasForeignKey(d => d.OrderId);
            this.HasRequired(t => t.PizzaType)
                .WithMany(t => t.PizzatypeOrderXrefs)
                .HasForeignKey(d => d.PizzatypeId);

        }
    }
}
