using System.Data.Entity.ModelConfiguration;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Repositories.Mapping
{
    public class PizzaMap : EntityTypeConfiguration<Pizza>
    {
        public PizzaMap()
        {
            // Primary Key
            this.HasKey(t => t.PizzaID);


            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Pizza");
            this.Property(t => t.PizzaID).HasColumnName("PizzaId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Image).HasColumnName("Image");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CategoryID).HasColumnName("CategoryId");

            // Relationships
            this.HasRequired(t => t.PizzaCategory)
                .WithMany(t => t.Pizzas)
                .HasForeignKey(d => d.CategoryID);

        }
    }
}
