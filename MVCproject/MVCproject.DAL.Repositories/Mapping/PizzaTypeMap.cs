using System.Data.Entity.ModelConfiguration;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Repositories.Mapping
{
    public class PizzaTypeMap : EntityTypeConfiguration<PizzaType>
    {
        public PizzaTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.PizzatypeId);

            // Properties
            // Table & Column Mappings
            this.ToTable("PizzaType");
            this.Property(t => t.PizzatypeId).HasColumnName("PizzatypeId");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.Weight).HasColumnName("Weight");
            this.Property(t => t.Diameter).HasColumnName("Diameter");
            this.Property(t => t.PizzaId).HasColumnName("PizzaId");

            // Relationships
            this.HasRequired(t => t.Pizza)
                .WithMany(t => t.PizzaTypes)
                .HasForeignKey(d => d.PizzaId);

            //this.HasMany(e => e.PizzatypeOrderXrefs).WithRequired(e => e.PizzaType).WillCascadeOnDelete(false);

        }
    }
}
