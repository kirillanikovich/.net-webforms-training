﻿using System;
using MVCproject.DAL.Entities.Models;

namespace MVCproject.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Pizza> Pizzas { get; }
        IRepository<PizzaType> PizzaTypes { get; }
        IRepository<PizzaCategory> PizzaCategories { get; }
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Role> Roles { get; }
        void Save();
    }
}
