﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVCproject.BLL.Models.Cart
{
    public class CartItem
    {
        [HiddenInput(DisplayValue = false)]
        public PizzaTypeDTO PizzaTypeDto { get; set; }

        [DisplayName("Название")]
        public string PizzaName { get; set; }

        [DisplayName("Количество")]
        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [Range(1, 10000, ErrorMessage = "Значение от 1 до 10000")]
        public int Amount { get; set; }
    }
}