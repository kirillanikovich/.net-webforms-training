﻿using System.Collections.Generic;
using System.Linq;

namespace MVCproject.BLL.Models.Cart

{
    public class Cart
    {
        private readonly List<CartItem> _cart = new List<CartItem>();

        public void AddItem(PizzaTypeDTO _pizzaTypeDto, int amount, string pizzaname)
        {
            CartItem element = _cart.FirstOrDefault(w => w.PizzaTypeDto.PizzaTypeID == _pizzaTypeDto.PizzaTypeID);

            if (element == null)
            {
                var item = new CartItem
                {
                    PizzaTypeDto = new PizzaTypeDTO()
                    {
                        PizzaID = _pizzaTypeDto .PizzaID,
                        PizzaTypeID = _pizzaTypeDto.PizzaTypeID,
                        Diameter = _pizzaTypeDto.Diameter,
                        Price = _pizzaTypeDto.Price,
                        Weight = _pizzaTypeDto.Weight
                    },
                    Amount = amount,
                    PizzaName = pizzaname
                };
                _cart.Add(item);
            }
            else
            {
                element.Amount += amount;
            }
        }

        public void RemoveItem(int id)
        {
            _cart.RemoveAll(r => r.PizzaTypeDto.PizzaTypeID == id);
        }

        //public decimal CalculateTotal()
        //{
        //    return _cart.Sum(s => s. * s.Amount);
        //}

        public void Clear()
        {
            _cart.Clear();
        }

        public IEnumerable<CartItem> GetCollection => _cart;
    }
}