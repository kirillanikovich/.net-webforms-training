﻿namespace MVCproject.BLL.Models
{
    public class RoleUserDTO
    {
        public int RoleID { get; set; }
        public int UserID { get; set; }
    }
}
