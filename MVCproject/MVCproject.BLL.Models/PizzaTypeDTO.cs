﻿namespace MVCproject.BLL.Models
{
    public class PizzaTypeDTO
    {
        public int PizzaTypeID { get; set; }
        public decimal Price { get; set; }
        public int Weight { get; set; }
        public int Diameter { get; set; }
        public int PizzaID { get; set; }
    }
}
