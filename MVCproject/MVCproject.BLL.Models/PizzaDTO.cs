﻿using System.Collections.Generic;

namespace MVCproject.BLL.Models
{
    public class PizzaDTO
    {
        public int PizzaID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string Description { get; set; }
        public int CategoryID { get; set; }
    }
}
