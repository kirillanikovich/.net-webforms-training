﻿namespace MVCproject.BLL.Models
{
    public class PizzatypeOrderXrefDTO
    {
        public int PizzaID { get; set; }
        public int OrderID { get; set; }
        public int Count { get; set; }
    }
}
