﻿using System;

namespace MVCproject.BLL.Models
{
    public class OrderDTO
    {
        public int OrderID { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public int UserID { get; set; }
    }
}
