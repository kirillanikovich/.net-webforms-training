﻿namespace MVCproject.BLL.Models
{
    public class PizzaCategoryDTO
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
    }
}
