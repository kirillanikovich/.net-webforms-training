﻿using System.Collections.Generic;

namespace MVCproject.BLL.Models
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public List<RoleDTO> Roles { get; set; }
    }
}
