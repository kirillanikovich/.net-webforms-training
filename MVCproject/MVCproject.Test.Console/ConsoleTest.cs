﻿using System.Linq;
using System.Text;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;

namespace MVCproject.Test.Console
{
    class ConsoleTest
    {
        static IUnitOfWork uof;
        
        static void Main(string[] args)
        {
            uof = new EfUnitOfWork("DefaultConnection");
            System.Console.OutputEncoding = Encoding.UTF8;

            ShowDatabase();
            DeleteMargarita(FindMargarita());
            ShowDatabase();
            CreateMargarita();
            ShowDatabase();
            EditMargarita();
            ShowDatabase();

            System.Console.ReadKey();
            System.Console.WriteLine("Press any key");
        }


        static void ShowDatabase()
        {
            foreach (PizzaCategory category in uof.PizzaCategories.Get())
            {
                System.Console.WriteLine("Категория: " + category.Name);
                foreach (Pizza pizza in category.Pizzas)
                {
                    System.Console.WriteLine("\t" + pizza.PizzaID + " - " + pizza.Name + " - " + pizza.Description);
                    foreach (PizzaType pizzatype in pizza.PizzaTypes)
                    {
                        System.Console.WriteLine("\t\t"+ pizzatype.PizzatypeId + " - " +
                            "Размер: " + pizzatype.Diameter + " - Масса: " + pizzatype.Weight + " - Цена: " + pizzatype.Price);
                    }
                }
            }
            System.Console.WriteLine("===================================================================================================");
        }

        static void EditMargarita()
        {
            Pizza pizza = uof.Pizzas.Get(p => p.Name == "Маргарита").Single();
            pizza.Description = "Отредактированное описание пиццы Маргарита";
            uof.Pizzas.Update(pizza);
            uof.Save();

        }

        static int FindMargarita()
        {
            Pizza pizza = uof.Pizzas.Get(p => p.Name == "Маргарита").Single();
            if (pizza != null)
            {
                return pizza.PizzaID;
            }

            return 0;
        }

        static void CreateMargarita()
        {
            uof.Pizzas.Insert(new Pizza()
            {
                PizzaCategory = uof.PizzaCategories.Get(cat => cat.Name == "Классические").Single(),
                CategoryID = uof.PizzaCategories.Get(cat => cat.Name == "Классические").Single().CategoryId,
                Name = "Маргарита",
                Description = "Описание пиццы : Маргарита",
                Image = null,
                PizzaTypes = null
            });

            uof.Save();

            int pizza_id = uof.Pizzas.Get(p => p.Name == "Маргарита").ToList().Single().PizzaID;

            uof.PizzaTypes.Insert(new PizzaType()
            {
                Diameter = 266,
                Weight = 2666,
                Price = 26,
                PizzaId = pizza_id
            });

            uof.PizzaTypes.Insert(new PizzaType()
            {
                Diameter = 277,
                Weight = 2777,
                Price = 27,
                PizzaId = pizza_id
            });

            uof.PizzaTypes.Insert(new PizzaType()
            {
                Diameter = 288,
                Weight = 2888,
                Price = 28,
                PizzaId = pizza_id
            });
            uof.Save();
        }

        static void DeleteMargarita(int id)
        {
            uof.Pizzas.Delete(id);
            uof.Save();
        }
    }
}
