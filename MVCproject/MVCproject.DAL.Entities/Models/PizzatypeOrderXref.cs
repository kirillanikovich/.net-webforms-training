namespace MVCproject.DAL.Entities.Models
{
    public class PizzatypeOrderXref
    {
        public int PizzatypeId { get; set; }
        public int OrderId { get; set; }
        public int Count { get; set; }
        public virtual Order Order { get; set; }
        public virtual PizzaType PizzaType { get; set; }
    }
}
