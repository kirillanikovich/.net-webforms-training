using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.DAL.Entities.Models
{
    public class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
            Roles = new HashSet<Role>();
        }

        public int UserId { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
