using System;
using System.Collections.Generic;

namespace MVCproject.DAL.Entities.Models
{
    public class Order
    {
        public Order()
        {
            PizzatypeOrderXrefs = new List<PizzatypeOrderXref>();
        }

        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public DateTime? Date { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PizzatypeOrderXref> PizzatypeOrderXrefs { get; set; }
    }
}
