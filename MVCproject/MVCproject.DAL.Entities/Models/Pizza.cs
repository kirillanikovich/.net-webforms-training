using System.Collections.Generic;

namespace MVCproject.DAL.Entities.Models
{
    public class Pizza
    {
        public Pizza()
        {
            PizzaTypes = new List<PizzaType>();
        }

        public int PizzaID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string Description { get; set; }
        public int CategoryID { get; set; }
        public virtual PizzaCategory PizzaCategory { get; set; }
        public virtual ICollection<PizzaType> PizzaTypes { get; set; }
    }
}
