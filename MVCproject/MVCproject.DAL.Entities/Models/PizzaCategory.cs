using System.Collections.Generic;

namespace MVCproject.DAL.Entities.Models
{
    public class PizzaCategory
    {
        public PizzaCategory()
        {
            Pizzas = new List<Pizza>();
        }

        public int CategoryId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Pizza> Pizzas { get; set; }
    }
}
