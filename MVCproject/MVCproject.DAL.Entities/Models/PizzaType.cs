using System.Collections.Generic;

namespace MVCproject.DAL.Entities.Models
{
    public class PizzaType
    {
        public PizzaType()
        {
            PizzatypeOrderXrefs = new List<PizzatypeOrderXref>();
        }

        public int PizzatypeId { get; set; }
        public decimal Price { get; set; }
        public int Weight { get; set; }
        public int Diameter { get; set; }
        public int PizzaId { get; set; }
        public virtual Pizza Pizza { get; set; }
        public virtual ICollection<PizzatypeOrderXref> PizzatypeOrderXrefs { get; set; }
    }
}
