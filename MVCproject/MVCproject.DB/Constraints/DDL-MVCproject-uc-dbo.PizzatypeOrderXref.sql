﻿--IF OBJECT_ID('FK_PizzatypeOrderXref_Pizza') IS NOT NULL
--	ALTER TABLE dbo.PizzatypeOrderXref
--		DROP CONSTRAINT FK_PizzatypeOrderXref_Pizza
--GO

--IF OBJECT_ID('FK_PizzatypeOrderXref_Order') IS NOT NULL
--	ALTER TABLE dbo.PizzatypeOrderXref
--		DROP CONSTRAINT FK_PizzatypeOrderXref_Order
--GO

ALTER TABLE dbo.PizzatypeOrderXref
	ADD CONSTRAINT FK_PizzatypeOrderXref_Pizza FOREIGN KEY (PizzaTypeId) REFERENCES dbo.PizzaType(PizzaTypeId) ON DELETE CASCADE
GO

ALTER TABLE dbo.PizzatypeOrderXref
	ADD CONSTRAINT FK_PizzatypeOrderXref_Order FOREIGN KEY (OrderId) REFERENCES dbo.[Order](OrderId) ON DELETE CASCADE
GO
