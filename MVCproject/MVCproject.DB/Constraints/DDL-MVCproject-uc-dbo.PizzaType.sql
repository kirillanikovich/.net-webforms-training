﻿--IF OBJECT_ID('[FK_PizzaType_Pizza]') IS NOT NULL
--	ALTER TABLE dbo.PizzaType
--		DROP CONSTRAINT [FK_PizzaType_Pizza]
--GO

ALTER TABLE dbo.PizzaType
	ADD CONSTRAINT [FK_PizzaType_Pizza] FOREIGN KEY ([PizzaId]) REFERENCES Pizza([PizzaId]) ON DELETE CASCADE
GO
