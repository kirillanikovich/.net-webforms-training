﻿--IF OBJECT_ID('[FK_Order_User]') IS NOT NULL
--	ALTER TABLE dbo.[Order]
--		DROP CONSTRAINT [FK_Order_User]
--GO

ALTER TABLE dbo.[Order]
	ADD CONSTRAINT [FK_Order_User] FOREIGN KEY ([UserId]) REFERENCES dbo.[User]([UserId])
GO
