﻿--IF OBJECT_ID('[FK_Pizza_PizzaCategory]') IS NOT NULL
--	ALTER TABLE dbo.Pizza
--		DROP CONSTRAINT [FK_Pizza_PizzaCategory]
--GO

ALTER TABLE dbo.Pizza
	ADD CONSTRAINT [FK_Pizza_PizzaCategory] FOREIGN KEY ([CategoryId]) REFERENCES dbo.PizzaCategory([CategoryId]) ON DELETE CASCADE
GO