﻿--IF OBJECT_ID('FK_RoleUserXref_Role') IS NOT NULL
--	ALTER TABLE dbo.RoleUserXref
--		DROP CONSTRAINT FK_RoleUserXref_Role
--GO

--IF OBJECT_ID('FK_RoleUserXref_User') IS NOT NULL
--	ALTER TABLE dbo.RoleUserXref
--		DROP CONSTRAINT FK_RoleUserXref_User
--GO

ALTER TABLE dbo.RoleUserXref
	ADD CONSTRAINT FK_RoleUserXref_Role FOREIGN KEY (RoleId) REFERENCES dbo.[Role](RoleId) ON DELETE CASCADE
GO

ALTER TABLE dbo.RoleUserXref
	ADD CONSTRAINT FK_RoleUserXref_User FOREIGN KEY (UserId) REFERENCES dbo.[User](UserId) 
GO
