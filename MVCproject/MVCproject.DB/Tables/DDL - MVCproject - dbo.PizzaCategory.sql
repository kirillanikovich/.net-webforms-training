﻿CREATE TABLE [dbo].[PizzaCategory]
(
	[CategoryId] INT NOT NULL IDENTITY(1,1), 
    [Name] NVARCHAR(25) NOT NULL,

	CONSTRAINT PK_PizzaCategory PRIMARY KEY ([CategoryId])	
)
