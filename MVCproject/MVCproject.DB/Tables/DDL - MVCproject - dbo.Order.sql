﻿CREATE TABLE [dbo].[Order]
(
	[OrderId] INT NOT NULL IDENTITY(1,1),
    [Name] NVARCHAR(255) NOT NULL, 
    [Adress] NVARCHAR(255) NOT NULL, 
    [Phone] NVARCHAR(20) NOT NULL, 
    [Date] DATETIME NULL, 
    [UserId] INT NOT NULL,

	CONSTRAINT PK_Order PRIMARY KEY ([OrderId])	
)

GO

