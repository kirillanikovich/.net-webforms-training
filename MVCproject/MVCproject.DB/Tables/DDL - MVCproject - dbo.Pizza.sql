﻿CREATE TABLE [dbo].[Pizza]
(
	[PizzaId] INT NOT NULL IDENTITY(1,1), 
    [Name] NVARCHAR(50) NOT NULL, 
    [Image] IMAGE NULL, 
    [Description] NVARCHAR(1000) NOT NULL, 
    [CategoryId] INT NOT NULL,

	CONSTRAINT PK_Pizza PRIMARY KEY ([PizzaId])	
)
