﻿CREATE TABLE [dbo].[PizzatypeOrderXref]
(
	[PizzatypeId] INT NOT NULL,
    [OrderId] INT NOT NULL,
	[Count] INT NOT NULL, 

    CONSTRAINT PK_PizzatypeOrderXref PRIMARY KEY ([PizzatypeId], [OrderId])
)
