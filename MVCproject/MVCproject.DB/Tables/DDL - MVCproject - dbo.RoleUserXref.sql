﻿CREATE TABLE [dbo].[RoleUserXref]
(
	[RoleId] INT NOT NULL,
    [UserId] INT NOT NULL,

    CONSTRAINT PK_RoleUserXref PRIMARY KEY ([RoleId], [UserId])
)
