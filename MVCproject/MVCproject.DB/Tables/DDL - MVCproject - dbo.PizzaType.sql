﻿CREATE TABLE [dbo].[PizzaType]
(
	[PizzatypeId] INT NOT NULL IDENTITY(1,1), 
    [Price] DECIMAL(6, 2) NOT NULL, 
    [Weight] INT NOT NULL, 
    [Diameter] INT NOT NULL, 
    [PizzaId] INT NOT NULL,

	CONSTRAINT PK_PizzaType PRIMARY KEY ([PizzatypeId])	
)
