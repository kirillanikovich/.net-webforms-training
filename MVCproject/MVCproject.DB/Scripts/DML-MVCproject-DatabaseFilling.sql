﻿DECLARE @INSERTED_ID INT
DECLARE @INSERTED_PIZZACATEGORY_ID INT
DECLARE @ID_TABLE TABLE(ID INT)

DELETE @ID_TABLE
INSERT INTO PizzaCategory([Name]) 
OUTPUT Inserted.CategoryID INTO @ID_TABLE 
VALUES(N'Классические')
SET @INSERTED_PIZZACATEGORY_ID = (SELECT ID from @ID_TABLE)

DELETE @ID_TABLE
INSERT INTO dbo.Pizza([CategoryID], [Name], [Description]) 
OUTPUT Inserted.PizzaID INTO @ID_TABLE 
VALUES (@INSERTED_PIZZACATEGORY_ID, N'Ветчина и грибы', N'Соус для пиццы, Ветчина, Сыр моцарелла, Шампиньоны. Ветчина и грибы – рецепт, полюбившийся многим по всему миру.')
SET @INSERTED_ID = (SELECT ID from @ID_TABLE)

DELETE @ID_TABLE
INSERT INTO dbo.PizzaType([PizzaID], [Diameter], [Price], [Weight]) 
OUTPUT Inserted.PizzaTypeID INTO @ID_TABLE 
VALUES (@INSERTED_ID, 20, 6, 350), (@INSERTED_ID, 28, 9, 500), (@INSERTED_ID, 36, 12, 800);

DELETE @ID_TABLE
INSERT INTO dbo.Pizza([CategoryID], [Name], [Description]) 
OUTPUT Inserted.PizzaID INTO @ID_TABLE 
VALUES (@INSERTED_PIZZACATEGORY_ID, N'Маргарита', N'Соус для пиццы, Сыр моцарелла. Маргарита – для любителей и ценителей классики.')
SET @INSERTED_ID = (SELECT ID from @ID_TABLE)

DELETE @ID_TABLE
INSERT INTO dbo.PizzaType([PizzaID], [Diameter], [Price], [Weight]) 
OUTPUT Inserted.PizzaTypeID INTO @ID_TABLE 
VALUES (@INSERTED_ID, 19, 5.5, 300), (@INSERTED_ID, 27, 8, 450), (@INSERTED_ID, 34, 11, 700);

DELETE @ID_TABLE
INSERT INTO dbo.Pizza([CategoryID], [Name], [Description]) 
OUTPUT Inserted.PizzaID INTO @ID_TABLE 
VALUES (@INSERTED_PIZZACATEGORY_ID, N'Пепперони', N'Соус для пиццы, Сыр моцарелла, Пепперони. Пепперони – идеальное сочетание сыра моцарелла и колбасы пепперони. ')
SET @INSERTED_ID = (SELECT ID from @ID_TABLE)

DELETE @ID_TABLE
INSERT INTO dbo.PizzaType([PizzaID], [Diameter], [Price], [Weight]) 
OUTPUT Inserted.PizzaTypeID INTO @ID_TABLE 
VALUES (@INSERTED_ID, 20, 7, 400), (@INSERTED_ID, 28, 9, 500), (@INSERTED_ID, 36, 12, 850);

INSERT INTO PizzaCategory([Name])
VALUES(N'Вегетарианские')


INSERT INTO [Role]([Name])
VALUES('User')
INSERT INTO [Role]([Name])
VALUES('Admin')


DECLARE @HashThis varchar(64);  
SET @HashThis = CONVERT(varchar(64),'dev');  
SELECT HASHBYTES('SHA1', @HashThis);  

INSERT INTO [User]([Email], [Login], [Password])
VALUES('Dev@project.com','dev', 'ef260e9aa3c673af240d17a2660480361a8e081d1ffeca2a5ed0e3219fc18567')
INSERT INTO [RoleUserXref]([RoleId],[UserId])
VALUES(2,1)
INSERT INTO [RoleUserXref]([RoleId],[UserId])
VALUES(1,1)

INSERT INTO [User]([Email], [Login], [Password])
VALUES('admin@project.com','admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918')
INSERT INTO [RoleUserXref]([RoleId],[UserId])
VALUES(2,2)

INSERT INTO [User]([Email], [Login], [Password])
VALUES('user@project.com','user', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb')
INSERT INTO [RoleUserXref]([RoleId],[UserId])
VALUES(1,3)
