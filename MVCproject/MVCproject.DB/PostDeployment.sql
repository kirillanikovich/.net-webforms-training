﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
--*/

:r .\Constraints\DDL-MVCproject-uc-dbo.Order.sql
:r .\Constraints\DDL-MVCproject-uc-dbo.Pizza.sql
:r .\Constraints\DDL-MVCproject-uc-dbo.PizzaType.sql
:r .\Constraints\DDL-MVCproject-uc-dbo.PizzatypeOrderXref.sql
:r .\Constraints\DDL-MVCproject-uc-dbo.RoleUserXref.sql

:r .\Scripts\DML-MVCproject-DatabaseFilling.sql