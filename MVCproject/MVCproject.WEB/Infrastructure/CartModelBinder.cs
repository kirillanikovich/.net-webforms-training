﻿using System.Web.Mvc;
using MVCproject.BLL.Models.Cart;

namespace MVCproject.WEB.Infrastructure
{
    public class CartModelBinder : IModelBinder
    {
        private const string Key = "Cart";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var cart = (Cart)controllerContext.HttpContext.Session[Key];

            if (cart != null)
            {
                return cart;
            }

            cart = new Cart();
            controllerContext.HttpContext.Session[Key] = cart;
            return cart;
        }
    }
}