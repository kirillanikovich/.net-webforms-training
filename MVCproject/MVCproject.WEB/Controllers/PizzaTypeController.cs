﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.WEB.Models;

namespace MVCproject.WEB.Controllers
{
    [RoutePrefix("Type")]
    [Authorize(Roles = "Admin")]
    public class PizzaTypeController : Controller
    {
        private readonly IPizzaTypeService _pizzaTypeService;

        public PizzaTypeController(IPizzaTypeService serv)
        {
            _pizzaTypeService = serv;
        }

        
        [Route("View")]
        [AllowAnonymous]
        public ActionResult View(int? pizzaid)
        {
            IEnumerable<PizzaTypeDTO> pizzaTypeDtos = _pizzaTypeService.GetPizzaTypesByPizzaId(pizzaid.Value);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaTypeDTO, PizzaTypeViewModel>()).CreateMapper();
            var pizzatypes = mapper.Map<IEnumerable<PizzaTypeDTO>, List<PizzaTypeViewModel>>(pizzaTypeDtos);
            ViewBag.pizzaid = pizzaid;
            return View(pizzatypes);
        }


        [HttpGet]
        [Route("Edit-{id}")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PizzaTypeDTO pizzaTypeDto = _pizzaTypeService.GetPizzaType(id);

            if (pizzaTypeDto != null)
            {
                PizzaTypeViewModel pizzaTypeViewModel = new PizzaTypeViewModel()
                {
                    PizzaID = pizzaTypeDto.PizzaID,
                    Diameter = pizzaTypeDto.Diameter,
                    Price = pizzaTypeDto.Price,
                    Weight = pizzaTypeDto.Weight,
                    PizzaTypeID = pizzaTypeDto.PizzaTypeID
                };

                return PartialView(pizzaTypeViewModel);
            }
            return HttpNotFound();
        }


        [HttpPost]
        [Route("Edit-{id}")]
        public ActionResult Edit(PizzaTypeViewModel pizzaTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                PizzaTypeDTO pizzaTypeDto = new PizzaTypeDTO()
                {
                    PizzaID = pizzaTypeViewModel.PizzaID,
                    Diameter = pizzaTypeViewModel.Diameter,
                    PizzaTypeID = pizzaTypeViewModel.PizzaTypeID,
                    Price = pizzaTypeViewModel.Price,
                    Weight = pizzaTypeViewModel.Weight
                };

                _pizzaTypeService.UpdatePizzaType(pizzaTypeDto);
                return RedirectToAction("Index", "PizzaCategory");
            }
            return View("Edit", pizzaTypeViewModel);
        }


        [HttpGet]
        [Route("Delete-{id}")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PizzaTypeDTO pizzaTypeDto = _pizzaTypeService.GetPizzaType(id);

            if (pizzaTypeDto != null)
            {
                PizzaTypeViewModel pizzaTypeViewModel = new PizzaTypeViewModel()
                {
                    PizzaID = pizzaTypeDto.PizzaID,
                    Diameter = pizzaTypeDto.Diameter,
                    Price = pizzaTypeDto.Price,
                    Weight = pizzaTypeDto.Weight,
                    PizzaTypeID = pizzaTypeDto.PizzaTypeID
                };

                return PartialView(pizzaTypeViewModel);
            }
            return RedirectToAction("Index", "PizzaCategory");
        }


        [Route("Delete-{id}")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PizzaTypeDTO pizzaTypeDto = _pizzaTypeService.GetPizzaType(id);
            if (pizzaTypeDto != null)
            {
                _pizzaTypeService.DeletePizzaType(id.Value);
            }
            return RedirectToAction("Index", "PizzaCategory");
        }


        [HttpGet]
        [Route("Create-{id}")]
        public ActionResult Create(int? id)
        {
            return PartialView(new PizzaTypeViewModel()
            {
                PizzaID = id.Value
            });
        }

        [HttpPost]
        [Route("Create-{id}")]
        public ActionResult Create(PizzaTypeViewModel pizzaTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                PizzaTypeDTO pizzaTypeDto = new PizzaTypeDTO()
                {
                    PizzaID = pizzaTypeViewModel.PizzaID,
                    Diameter = pizzaTypeViewModel.Diameter,
                    PizzaTypeID = pizzaTypeViewModel.PizzaTypeID,
                    Price = pizzaTypeViewModel.Price,
                    Weight = pizzaTypeViewModel.Weight
                };

                _pizzaTypeService.CreatePizzaType(pizzaTypeDto);
                return RedirectToAction("Index", "PizzaCategory");
            }
            return PartialView("Create", pizzaTypeViewModel);
        }

    }
}
