﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.WEB.Models;

namespace MVCproject.WEB.Controllers
{
    [RoutePrefix("Categories")]
    [Authorize(Roles = "Admin")]
    public class PizzaCategoryController : Controller
    {
        private readonly IPizzaCategoryService _pizzaCategoryService;

        public PizzaCategoryController(IPizzaCategoryService serv)
        {
            _pizzaCategoryService = serv;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            IEnumerable<PizzaCategoryDTO> pizzaCategoryDtos = _pizzaCategoryService.GetPizzaCategories();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaCategoryDTO, PizzaCategoryViewModel>()).CreateMapper();
            var pizzaCategories = mapper.Map<IEnumerable<PizzaCategoryDTO>, ICollection<PizzaCategoryViewModel>>(pizzaCategoryDtos);
            return View(pizzaCategories);
        }

        [Route("Create")]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [Route("Create")]
        public ActionResult Create(PizzaCategoryViewModel pizzaCategoryViewModel)
        {
            if (ModelState.IsValid)
            {
                _pizzaCategoryService.CreatePizzaCategory(new PizzaCategoryDTO()
                {
                    Name = pizzaCategoryViewModel.Name
                });
            }
            return RedirectToAction("Index");
        }


        [Route("Edit")]
        public ActionResult Edit()
        {
            IEnumerable<PizzaCategoryDTO> pizzaCategoryDtos = _pizzaCategoryService.GetPizzaCategories();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaCategoryDTO, PizzaCategoryViewModel>()).CreateMapper();
            var pizzaCategories = mapper.Map<IEnumerable<PizzaCategoryDTO>, List<PizzaCategoryViewModel>>(pizzaCategoryDtos);
            return View(pizzaCategories);
        }


        [HttpPost]
        [Route("Edit")]
        public ActionResult Edit(List<PizzaCategoryViewModel> pizzaCategoryViewModels)
        {
            if (ModelState.IsValid)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaCategoryDTO, PizzaCategoryViewModel>())
                    .CreateMapper();
                var pizzaCategories =
                    mapper.Map<IEnumerable<PizzaCategoryViewModel>, ICollection<PizzaCategoryDTO>>(
                        pizzaCategoryViewModels);

                _pizzaCategoryService.UpdatePizzaCategories(pizzaCategories);
                return RedirectToAction("Index");
            }
            return View("Edit", pizzaCategoryViewModels);
        }


        [HttpGet]
        [Route("Delete-{name}")]
        public ActionResult Delete(string name)
        {
            if (name != null)
            {
                PizzaCategoryDTO pizzaCategoryDto =
                    _pizzaCategoryService.GetPizzaCategories().Single(t => t.Name == name);

                return PartialView("Delete", new PizzaCategoryViewModel()
                    {
                        CategoryID = pizzaCategoryDto.CategoryID,
                        Name = pizzaCategoryDto.Name
                    }
                );
            }
            return HttpNotFound();
        }


        [HttpPost, ActionName("Delete")]
        [Route("Delete-{name}")]
        public ActionResult DeleteConfirm(string name)
        {
            _pizzaCategoryService.DeletePizzaCategory(_pizzaCategoryService.GetPizzaCategories().Single(t => t.Name == name).CategoryID);
            return RedirectToAction("Index", "PizzaCategory");
        }

        [HttpGet]
        [Route("GetSelectList-{categoryid}")]
        public PartialViewResult GetSelectList(int? categoryid = null)
        {
            IEnumerable<PizzaCategoryDTO> pizzaCategoryDtos = _pizzaCategoryService.GetPizzaCategories();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaCategoryDTO, PizzaCategoryViewModel>()).CreateMapper();
            SelectList sl = new SelectList(mapper.Map<IEnumerable<PizzaCategoryDTO>, ICollection<PizzaCategoryViewModel>>(pizzaCategoryDtos), "CategoryId", "Name");
            if (categoryid.HasValue)
            {
                foreach (var item in sl)
                {
                    if (item.Value == categoryid.ToString())
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            return PartialView(sl);
        }
    }
}
