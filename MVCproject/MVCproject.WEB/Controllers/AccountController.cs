﻿using System;
using System.Collections.Generic;
using System.Web;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.WEB.Models.UserModels;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using MVCproject.WEB.Auth;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MVCproject.WEB.Controllers
{

    [RoutePrefix("")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService serv)
        {
            _userService = serv;
        }

        [HttpGet]
        [Route("Login")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, bool relogin = false)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDTO;
                userDTO = _userService.GetUser(model.Login);
                if (userDTO != null)
                {

                    string checkPassword;
                    if (relogin)
                        checkPassword = model.Password;
                    else
                        checkPassword = GetPasswordHash(model.Password);

                    if (userDTO.Password == checkPassword)
                    {
                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel()
                        {
                            Email = userDTO.Email,
                            Login = userDTO.Login,
                            UserId = userDTO.UserId,
                            Roles = userDTO.Roles.Select(r => r.Name).ToArray()
                        };
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        string userData = serializer.Serialize(serializeModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            1,
                            model.Login,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(15),
                            false,
                            userData);

                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie Cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(Cookie);

                        return Json(new
                        {
                            status=true
                        });
                    }
                }
                ModelState.AddModelError("Error", "Неверный логин или пароль.");
                return PartialView(model);
            }
            return HttpNotFound();
        }

        [HttpGet]
        [Route("Register")]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [Route("Register")]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDTO = _userService.GetUser(model.Login);

                if (userDTO == null)
                {
                    _userService.CreateUser(new UserDTO()
                    {
                        Email = model.Email,
                        Login = model.Login,
                        Password = GetPasswordHash(model.Password)
                    });

                    userDTO = _userService.GetUser(model.Login);
                    if (userDTO != null)
                    {
                        Login(new LoginViewModel()
                        {
                            Login = model.Login,
                            Password = model.Password
                        });
                        return RedirectToAction("Index", "PizzaCategory");
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", "Пользователь с таким логином уже существует");
                }
            }
            return View(model);
        }

        [HttpGet]
        [Route("Logout")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "PizzaCategory");
        }

        [HttpGet]
        [Route("Profile-{login}")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult Profile(string login)
        {
            if (login == null)
            {
                return HttpNotFound();
            }
            UserDTO userDTO = _userService.GetUser(login);

            if (userDTO != null)
            {
                RegisterViewModel registerViewModel = new RegisterViewModel()
                {
                    Login = userDTO.Login,
                    Email = userDTO.Email
                };
                return View(registerViewModel);
            }
            return HttpNotFound();
        }

        [HttpGet]
        [Route("Edit-{login}")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult Edit(string login)
        {
            if (login == null)
            {
                return HttpNotFound();
            }
            UserDTO userDTO = _userService.GetUser(login);

            if (userDTO != null)
            {
                ProfileViewModel profileViewModel = new ProfileViewModel()
                {
                    UserId = userDTO.UserId,
                    Login = userDTO.Login,
                    Email = userDTO.Email
                };
                return View(profileViewModel);
            }
            return HttpNotFound();
        }


        [HttpPost]
        [Route("Edit-{name}")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult Edit(ProfileViewModel profileViewModel, string name)
        {
            if (ModelState.IsValid)
            {
                if (_userService.GetUser(profileViewModel.Login) == null || profileViewModel.Login == _userService.GetUser(name).Login)
                { 
                    UserDTO userDTO = _userService.GetUser(profileViewModel.UserId);
                
                    userDTO.Login = profileViewModel.Login;
                    userDTO.Email = profileViewModel.Email;
                    _userService.UpdateUser(userDTO);
                    Logout();
                    Login(new LoginViewModel()
                    {
                        Login = userDTO.Login,
                        Password = userDTO.Password
                    }, true);
                    return RedirectToAction("Index", "PizzaCategory");
                }
                else
                {
                    ModelState.AddModelError("Error", "Пользователь с таким логином уже существует");
                }
            }
            return View(profileViewModel);
        }
        

        [HttpGet]
        [Route("ChangePassword-{login}")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult ChangePassword(string login)
        {
            if (login == null)
            {
                return HttpNotFound();
            }
            UserDTO userDTO = _userService.GetUser(login);

            if (userDTO != null)
            {
                PasswordViewModel passwordViewModel = new PasswordViewModel()
                {
                    UserId = userDTO.UserId
                };
                return View(passwordViewModel);
            }
            return HttpNotFound();
        }
        

        [HttpPost]
        [Route("ChangePassword-{name}")]
        [Authorize(Roles = "User, Admin")]
        public ActionResult ChangePassword(PasswordViewModel passwordViewModel)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDTO = _userService.GetUser(passwordViewModel.UserId);
                if (userDTO.Password == GetPasswordHash(passwordViewModel.CurrentPassword))
                {
                    userDTO.Password = GetPasswordHash(passwordViewModel.NewPassword);
                    _userService.UpdateUser(userDTO);
                    Logout();
                    Login(new LoginViewModel()
                    {
                        Login = userDTO.Login,
                        Password = passwordViewModel.NewPassword
                    });
                    return RedirectToAction("Index", "PizzaCategory");
                }
                ModelState.AddModelError("Error", "Текущий пароль неверный!");
            }
            return View("ChangePassword", passwordViewModel);
        }

        public string GetPasswordHash(string password)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            { 
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));
  
                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
