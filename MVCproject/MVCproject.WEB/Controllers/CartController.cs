﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.BLL.Models.Cart;
using MVCproject.WEB.Auth;
using MVCproject.WEB.Models;

namespace MVCproject.WEB.Controllers
{

    [RoutePrefix("Cart")]
    [Authorize(Roles = "User")]
    public class CartController : Controller
    {
        private readonly IPizzaTypeService _pizzaTypeService;
        private readonly IOrderService _orderService;

        public CartController(IPizzaTypeService pizzaTypeService, IOrderService orderService)
        {
            _pizzaTypeService = pizzaTypeService;
            _orderService = orderService;
        }


        [HttpGet]
        [Route("All"), ActionName("List")]
        public ActionResult List(Cart cart)
        {
            return View(cart);
        }


        [HttpGet]
        [Route("AddToCart")]
        public ActionResult AddToCart(int? id)
        {
            return PartialView(new CartItem()
            {
                PizzaTypeDto = new PizzaTypeDTO()
                {
                    PizzaTypeID = id.Value
                }
            });
        }


        [HttpPost, ActionName("AddToCart")]
        [Route("AddToCart")]
        public JsonResult AddToCart(Cart cart, int? id, int? amount)
        {
            var product = _pizzaTypeService.GetPizzaType(id);
            var pizzaname = _pizzaTypeService.GetPizzaNameByPizzaTypeId(id);
            if (product != null)
            {
                cart.AddItem(product, amount.Value, pizzaname);
            }
            return Json(cart, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Route("RemoveFromCart-{id}")]
        public ActionResult RemoveFromCart(Cart cart, int id)
        {
            cart.RemoveItem(id);
            return RedirectToAction("List", cart);
        }


        [HttpGet]
        [Route("Checkout")]
        public ActionResult Checkout(Cart cart)
        {
            return View(new OrderViewModel());
        }

         public ViewResult Calculate(Cart cart)
        {
            decimal sum = 0;

            foreach (CartItem item in cart.GetCollection)
            {
                sum += item.Amount * item.PizzaTypeDto.Price;
            }

            return View(sum);
        }

        
        [HttpPost]
        [Route("Checkout")]
        public ActionResult Checkout(Cart cart, OrderViewModel details)
        {
            if (!cart.GetCollection.Any())
            {
                ModelState.AddModelError("", "Корзина пуста!");
            }
            if (ModelState.IsValid)
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData); var order = new OrderDTO
                    {
                        Name = details.Name,
                        Date = DateTime.Now,
                        Adress = details.Adress,
                        Phone = details.Phone,
                        UserID = serializeModel.UserId
                    };
                    _orderService.MakeOrder(cart.GetCollection, order);

                    cart.Clear();
                    return RedirectToAction("Index", "PizzaCategory");
                    
                }
            }
            return View(details);
        }
    }
}
