﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.WEB.Models.UserModels;

namespace MVCproject.WEB.Controllers
{

    [RoutePrefix("Admin")]
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;

        public AdminController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("Users")]
        public ActionResult Users()
        {
            List<UserDTO> userDTOs = _userService.GetUsers();
            return View(userDTOs);
        }


        [HttpGet]
        [Route("Edit-{id}")]
        public ActionResult Edit(int id)
        {
            UserDTO userDTO = _userService.GetUser(id);


            List<RoleDTO> roleDTOs = _userService.GetRoles();
            List<RoleViewModel> roleViewModels = new List<RoleViewModel>();

            if (roleDTOs != null && userDTO != null)
            {
                foreach (var role in roleDTOs)
                {
                    roleViewModels.Add(new RoleViewModel()
                    {
                        Name = role.Name,
                        RoleId = role.RoleId,
                        isChecked = userDTO.Roles.Exists(t => t.Name == role.Name)
                    });
                }

                return View(new UserViewModel()
                {
                    Login = userDTO.Login,
                    Email = userDTO.Email,
                    UserId = userDTO.UserId,
                    Roles = roleViewModels
                });
            }
            return RedirectToAction("Users", "Admin");
        }


        [HttpPost]
        [Route("Edit-{id}")]
        public ActionResult Edit(UserViewModel userViewModel, int id)
        {
            if (ModelState.IsValid)
            {
                if (_userService.GetUser(userViewModel.Login) == null || userViewModel.Login == _userService.GetUser(id).Login)
                {
                    List<RoleDTO> roleDTOs = new List<RoleDTO>();
                    foreach (var role in userViewModel.Roles)
                    {
                        if (role.isChecked)
                        {
                            roleDTOs.Add(new RoleDTO()
                            {
                                Name = role.Name,
                                RoleId = role.RoleId
                            });
                        }
                    }

                    UserDTO userDTO = new UserDTO()
                    {
                        Login = userViewModel.Login,
                        Email = userViewModel.Email,
                        UserId = userViewModel.UserId,
                        Roles = roleDTOs
                    };
                    _userService.ChangeRoles(userDTO);
                    return RedirectToAction("Users", "Admin");
                }
                else
                {
                    ModelState.AddModelError("Error", "Пользователь с таким логином уже существует");
                }
            }
            return View(userViewModel);
        }


        [HttpGet]
        [Route("Delete-{id}")]
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UserDTO userDTO = _userService.GetUser(id);

            if (userDTO != null)
            {
                UserViewModel userViewModel = new UserViewModel()
                {
                    Login = userDTO.Login
                };

                return PartialView(userViewModel);
            }
            return RedirectToAction("Users", "Admin");
        }


        [Route("Delete-{id}")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UserDTO userDTO = _userService.GetUser(id);

            if (userDTO != null)
            {
                _userService.DeleteUser(id);
            }
            return RedirectToAction("Users", "Admin");
        }


        [HttpGet]
        [Route("Webapi")]
        public ActionResult Webapi()
        {
           

            return View();
        }


        
    }
}
