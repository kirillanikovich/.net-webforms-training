﻿using System.Collections.Generic;
using System.Web.Http;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;

namespace MVCproject.WEB.Controllers.WebAPI
{
    public class PizzaTypeApiController : ApiController
    {
        private readonly IPizzaTypeService _pizzaTypeService;
        private readonly IPizzaService _pizzaService;

        public PizzaTypeApiController(IPizzaTypeService pizzaTypeService, IPizzaService pizzaService)
        {
            _pizzaTypeService = pizzaTypeService;
            _pizzaService = pizzaService;
        }

        [HttpGet]
        public PizzaTypeDTO GetPizzaType(int id)
        {
            PizzaTypeDTO pizzaTypeDto = _pizzaTypeService.GetPizzaType(id);
            return pizzaTypeDto;
        }

        [HttpGet]
        public IEnumerable<PizzaDTO> GetPizzas()
        {
            return _pizzaService.GetPizzas();
        }

        [HttpGet]
        public IEnumerable<PizzaTypeDTO> GetPizzaTypes()
        {
            return _pizzaTypeService.GetPizzaTypes();
        }

        [HttpPost]
        public void CreatePizzaType([FromBody]PizzaTypeDTO pizzaTypeDto)
        {
            _pizzaTypeService.CreatePizzaType(pizzaTypeDto);
        }

        [HttpPut]
        public void EditPizzaType(int id, [FromBody]PizzaTypeDTO pizzaTypeDto)
        {
            if (id == pizzaTypeDto.PizzaTypeID)
            {
                _pizzaTypeService.UpdatePizzaType(pizzaTypeDto);
            }
        }

        public void DeletePizzaType(int id)
        {
            _pizzaTypeService.DeletePizzaType(id);
        }

        
    }
}
