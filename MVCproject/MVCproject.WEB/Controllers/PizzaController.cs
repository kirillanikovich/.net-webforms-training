﻿using System;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MVCproject.WEB.Models;

namespace MVCproject.WEB.Controllers
{
    [RoutePrefix("Pizza")]
    [Authorize(Roles = "Admin")]
    public class PizzaController : Controller
    {
        private readonly IPizzaService _pizzaService;

        public PizzaController(IPizzaService serv)
        {
           _pizzaService =  serv;
        }


        [Route("All")]
        public ActionResult Index(int i = 0)
        {
            IEnumerable<PizzaDTO> pizzaDtos = _pizzaService.GetPizzas();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaDTO, PizzaViewModel>()).CreateMapper();
            var pizzas = mapper.Map<IEnumerable<PizzaDTO>, ICollection<PizzaViewModel>>(pizzaDtos);
            return View(pizzas);
        }

        [AllowAnonymous]
        public PartialViewResult List(int categoryid = 0)
        {
            IEnumerable<PizzaDTO> pizzaDtos;
            if (categoryid == 0)
            {
                pizzaDtos = _pizzaService.GetPizzas();
            }
            else
            {
                pizzaDtos = _pizzaService.GetPizzas().Where(pizza => pizza.CategoryID == categoryid);
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaDTO, PizzaViewModel>()).CreateMapper();
            var pizzas = mapper.Map<IEnumerable<PizzaDTO>, ICollection<PizzaViewModel>>(pizzaDtos);
            return PartialView(pizzas);
        }
        

        [HttpGet]
        [Route("Edit-{name}")]
        public ActionResult Edit(string name)
        {
            if (name == null)
            {
                return HttpNotFound();
            }
            PizzaDTO pizzaDto = _pizzaService.GetPizzas().Single(pizza => pizza.Name == name);
            
            if (pizzaDto != null)
            {

                PizzaViewModel pizzaviewmodel = new PizzaViewModel()
                {
                    PizzaID = pizzaDto.PizzaID,
                    Name = pizzaDto.Name,
                    CategoryID = pizzaDto.CategoryID,
                    Description = pizzaDto.Description,
                    Image = pizzaDto.Image
                };

                return View(pizzaviewmodel);
            }
            return HttpNotFound();
        }

        
        [HttpPost]
        [Route("Edit-{name}")]
        public ActionResult Edit(PizzaViewModel pizzaViewModel, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                PizzaDTO pizzaDto = new PizzaDTO()
                {
                    PizzaID = pizzaViewModel.PizzaID,
                    Name = pizzaViewModel.Name,
                    CategoryID = pizzaViewModel.CategoryID,
                    Description = pizzaViewModel.Description,
                    Image = pizzaViewModel.Image
                };

                if (uploadImage != null)
                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        pizzaDto.Image = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }
                    else
                    {
                        pizzaDto.Image = _pizzaService.GetPizza(pizzaDto.PizzaID).Image;
                    }

                _pizzaService.UpdatePizza(pizzaDto);
                return RedirectToAction("Index", "PizzaCategory");
            }
            return View("Edit", pizzaViewModel);
        }


        [Route("Create-{id}")]
        public ActionResult Create(int? id)
        {
            return View(new PizzaViewModel()
            {
                CategoryID = id.HasValue ? id.Value : 0
            }
            );
        }


        [HttpPost]
        [Route("Create-{id}")]
        public ActionResult Create(PizzaViewModel pizzaViewModel, HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid)
            {
                byte[] imageData = null;
                if (uploadImage != null)
                {
                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }
                }

                PizzaDTO pizzaDto = new PizzaDTO()
                {
                    CategoryID = pizzaViewModel.CategoryID,
                    Description = pizzaViewModel.Description,
                    Name = pizzaViewModel.Name,
                    Image = imageData
                };
                _pizzaService.CreatePizza(pizzaDto);
            }
            return RedirectToAction("Index", "PizzaCategory");
        }
        

        [HttpGet]
        [Route("Delete-{name}")]
        public ActionResult Delete(string name)
        {
            if (name == null)
            {
                return HttpNotFound();
            }

            PizzaDTO pizzaDto = _pizzaService.GetPizzaByName(name);

            if (pizzaDto != null)
            {
                PizzaViewModel pizzaviewmodel = new PizzaViewModel()
                {
                    PizzaID = pizzaDto.PizzaID,
                    Name = pizzaDto.Name,
                    CategoryID = pizzaDto.CategoryID,
                    Description = pizzaDto.Description,
                    Image = pizzaDto.Image
                };

                return PartialView(pizzaviewmodel);
            }
            return RedirectToAction("Index", "PizzaCategory");
        }


        [Route("Delete-{name}")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string name)
        {
            if (name == null)
            {
                return HttpNotFound();
            }

            PizzaDTO pizzaDto = _pizzaService.GetPizzaByName(name);
            if (pizzaDto != null)
            {
                _pizzaService.DeletePizza(pizzaDto.PizzaID);
            }
            return RedirectToAction("Index", "PizzaCategory");
        }
        

        
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PizzaDTO pizzaDto = _pizzaService.GetPizza(id);


            if (pizzaDto != null)
            {

                PizzaViewModel pizzaviewmodel = new PizzaViewModel()
                {
                    PizzaID = pizzaDto.PizzaID,
                    Name = pizzaDto.Name,
                    CategoryID = pizzaDto.CategoryID,
                    Description = pizzaDto.Description,
                    Image = pizzaDto.Image
                };

                return View(pizzaviewmodel);
            }
            return HttpNotFound();
        }


    }
}