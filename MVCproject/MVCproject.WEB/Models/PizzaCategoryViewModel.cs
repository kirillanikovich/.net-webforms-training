﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVCproject.WEB.Models
{
    public class PizzaCategoryViewModel
    {
        [Required]
        [HiddenInput(DisplayValue = false)]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [MaxLength(25, ErrorMessage = "Длина поля не должна превышать 25 символов.")]
        [DisplayName("Категория")]
        public string Name { get; set; }
    }
}