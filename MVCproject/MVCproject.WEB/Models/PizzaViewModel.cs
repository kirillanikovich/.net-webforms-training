﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVCproject.WEB.Models
{
    public class PizzaViewModel
    {
        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [HiddenInput(DisplayValue = false)]
        public int PizzaID { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DisplayName("Название")]
        [MaxLength(50, ErrorMessage = "Длина поля не должна превышать 25 символов.")]
        public string Name { get; set; }

        public byte[] Image { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DisplayName("Описание")]
        [MaxLength(1000, ErrorMessage = "Длина поля не должна превышать 25 символов.")]
        public string Description { get; set; }

        [Required]
        [HiddenInput(DisplayValue = false)]
        public int CategoryID { get; set; }

    }
}