﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVCproject.WEB.Models
{
    public class PizzaTypeViewModel
    {
        [Required]
        [HiddenInput(DisplayValue = false)]
        public int PizzaTypeID { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DisplayName("Цена")]
        [Range(1, 10000, ErrorMessage = "Значение от 1 до 10000")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DisplayName("Масса")]
        [Range(1, 10000, ErrorMessage = "Значение от 1 до 10000")]
        public int Weight { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DisplayName("Диаметр")]
        [Range(1, 10000, ErrorMessage = "Значение от 1 до 10000")]
        public int Diameter { get; set; }

        [Required]
        [HiddenInput(DisplayValue = false)]
        public int PizzaID { get; set; }
    }
}