﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.WEB.Models
{
    public class OrderViewModel
    {
        public int OrderID { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [MaxLength(64, ErrorMessage = "Длина поля не должна превышать 64 символов.")]
        [DisplayName("Ваше имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [MaxLength(100, ErrorMessage = "Длина поля не должна превышать 25 символов.")]
        [DisplayName("Адрес")]
        public string Adress { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [RegularExpression(@"^(\d{9})$", ErrorMessage = "Номер телефона в формате 'XXYYYYYYY', где XX - код оператора, YYYYYYY - номер.")]
        [DisplayName("Телефон")]
        public string Phone { get; set; }

        public DateTime Date { get; set; }

        public int UserID { get; set; }
    }
}