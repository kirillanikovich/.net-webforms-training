﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.WEB.Models.UserModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [MaxLength(25, ErrorMessage = "Длина поля не должна превышать 25 символов.")]
        [DisplayName("Логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DataType(DataType.Password)]
        [DisplayName("Пароль")]
        public string Password
        {
            get; set;
        }
    }
}
