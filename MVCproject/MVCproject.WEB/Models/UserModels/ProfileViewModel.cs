﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.WEB.Models.UserModels
{
    public class ProfileViewModel
    {
        [Required]
        public int UserId { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [MaxLength(64, ErrorMessage = "Длина поля не должна превышать 64 символов.")]
        [DisplayName("Логин")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [MaxLength(100, ErrorMessage = "Длина поля не должна превышать 100 символов.")]
        [DisplayName("Email")]
        public string Email { get; set; }
    }
}