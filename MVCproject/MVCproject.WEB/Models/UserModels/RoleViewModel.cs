﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.WEB.Models.UserModels
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }
        
        public string Name { get; set; }

        public bool isChecked { get; set; }
    }
}