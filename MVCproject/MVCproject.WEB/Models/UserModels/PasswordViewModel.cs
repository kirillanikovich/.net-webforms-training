﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCproject.WEB.Models.UserModels
{
    public class PasswordViewModel
    {
        [Required]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DataType(DataType.Password)]
        [DisplayName("Текущий пароль")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DataType(DataType.Password)]
        [DisplayName("Пароль")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DisplayName("Повтор пароля")]
        public string ConfirmPassword { get; set; }
    }
}