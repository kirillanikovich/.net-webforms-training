﻿using System.Linq;
using System.Security.Principal;

namespace MVCproject.WEB.Auth
{
    public class CustomPrincipal : IPrincipal
    {

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            return false;
        }


        public CustomPrincipal(string login)
        {
            this.Identity = new GenericIdentity(login);
        }
    }
}