﻿namespace MVCproject.WEB.Auth
{
    public class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
    }
}