﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models.Cart;
using MVCproject.BLL.Services;
using MVCproject.WEB.Auth;
using MVCproject.WEB.Infrastructure;
using Ninject;
using Ninject.Web.Common.WebHost;


namespace MVCproject.WEB
{
    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());

        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IPizzaService>().To<PizzaService>();
            kernel.Bind<IPizzaCategoryService>().To<PizzaCategoryService>();
            kernel.Bind<IPizzaTypeService>().To<PizzaTypeService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IUserService>().To<UserService>();

            GlobalConfiguration.Configuration.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(kernel);

            return kernel;
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserId = serializeModel.UserId;
                newUser.Login = serializeModel.Login;
                newUser.Email = serializeModel.Email;
                newUser.Roles = serializeModel.Roles.ToArray<string>();
                
                HttpContext.Current.User = newUser;
            }
        }
    }
}