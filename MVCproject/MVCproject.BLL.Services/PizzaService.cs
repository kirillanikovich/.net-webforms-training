﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using MVCproject.BLL.Infrastructure;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;


namespace MVCproject.BLL.Services
{
    public class PizzaService : IPizzaService
    {
        public IUnitOfWork Database { get; set; }

        public PizzaService(string connectionString = "DefaultConnection")
        {
            Database = new EfUnitOfWork(connectionString);
        }

        public PizzaService(IService service)
        {
            Database = service.Database;
        }


        public PizzaDTO GetPizza(int? id = 1)
        {
            if (id == null)
                throw new ValidationException("pizza id is null", "");
            var pizza = Database.Pizzas.GetEntityById(id.Value);
            if (pizza == null)
                throw new ValidationException("Pizza not found", "");
            
            return new PizzaDTO
            {
                Name = pizza.Name,
                PizzaID = pizza.PizzaID,
                Image = pizza.Image,
                Description = pizza.Description,
                CategoryID = pizza.CategoryID
            };
        }
          
        public IEnumerable<PizzaDTO> GetPizzas()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Pizza, PizzaDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Pizza>, List<PizzaDTO>>(Database.Pizzas.Get());
        }
        
        public void UpdatePizza(PizzaDTO model)
        {
            Pizza pizza = new Pizza
            {
                PizzaID = model.PizzaID,
                CategoryID = model.CategoryID,
                Name = model.Name,
                Description = model.Description,
                Image = model.Image
            };
            foreach (var pizzaType in pizza.PizzaTypes)
            {
                pizzaType.Pizza = pizza;
            }
            Database.Pizzas.Update(pizza);
            Database.Save();
        }

        public void CreatePizza(PizzaDTO model)
        {
            Pizza pizza = new Pizza
            {
                PizzaID = model.PizzaID,
                CategoryID = model.CategoryID,
                Name = model.Name,
                Description = model.Description,
                Image = model.Image
            };
            Database.Pizzas.Insert(pizza);
            Database.Save();
        }

        public void DeletePizza(int id)
        {
            Database.Pizzas.Delete(id);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public PizzaDTO GetPizzaByName(string name)
        {
            Pizza pizza = Database.Pizzas.Get(t => t.Name == name).Single();
            return new PizzaDTO
            {
                Name = pizza.Name,
                PizzaID = pizza.PizzaID,
                Image = pizza.Image,
                Description = pizza.Description,
                CategoryID = pizza.CategoryID
            };
        }
    }
}
