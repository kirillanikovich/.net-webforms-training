﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;

namespace MVCproject.BLL.Services
{
    public class PizzaCategoryService : IPizzaCategoryService
    {
        public IUnitOfWork Database { get; set; }

        public PizzaCategoryService(string connectionString = "DefaultConnection")
        {
            Database = new EfUnitOfWork(connectionString);
        }

        public PizzaCategoryService(IService service)
        {
            Database = service.Database;
        }
        

        public void CreatePizzaCategory(PizzaCategoryDTO model)
        {
            Database.PizzaCategories.Insert(new PizzaCategory()
            {
                Name = model.Name
            });
            Database.Save();
        }

        
        public PizzaCategoryDTO GetPizzaCategory(int? id)
        {
            PizzaCategory pizzaCategory = Database.PizzaCategories.GetEntityById(id);

            return new PizzaCategoryDTO()
            {
                CategoryID = pizzaCategory.CategoryId, Name = pizzaCategory.Name
            };
        }


        public IEnumerable<PizzaCategoryDTO> GetPizzaCategories()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaCategory, PizzaCategoryDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<PizzaCategory>, List<PizzaCategoryDTO>>(Database.PizzaCategories.Get());
        }


        public void UpdatePizzaCategories(ICollection<PizzaCategoryDTO> pizzaCategoryDtos)
        {
            foreach (var pizzaCategoryDto in pizzaCategoryDtos)
            {
                PizzaCategory pizzaCategory = new PizzaCategory()
                {
                    Name = pizzaCategoryDto.Name,
                    CategoryId = pizzaCategoryDto.CategoryID
                };
                Database.PizzaCategories.Update(pizzaCategory);
            }
            Database.Save();
        }

        public void DeletePizzaCategory(int id)
        {
            Database.PizzaCategories.Delete(id);
            Database.Save();
        }


        public void Dispose()
        {
            Database.Dispose();
        }

    }
}
