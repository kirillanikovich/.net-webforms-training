﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;

namespace MVCproject.BLL.Services
{
    public class PizzaTypeService : IPizzaTypeService
    {
        public IUnitOfWork Database { get; set; }

        public PizzaTypeService(string connectionString = "DefaultConnection")
        {
            Database = new EfUnitOfWork(connectionString);
        }

        public PizzaTypeService(IService service)
        {
            Database = service.Database;
        }

        public void UpdatePizzaType(PizzaTypeDTO pizzaTypeDto)
        {
            PizzaType pizzaType = new PizzaType()
            {
                Diameter = pizzaTypeDto.Diameter,
                Price = pizzaTypeDto.Price,
                Weight = pizzaTypeDto.Weight,
                PizzaId = pizzaTypeDto.PizzaID,
                PizzatypeId = pizzaTypeDto.PizzaTypeID
            };
            Database.PizzaTypes.Update(pizzaType);
            Database.Save();
        }


        public void DeletePizzaType(int id)
        {
            Database.PizzaTypes.Delete(id);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public PizzaTypeDTO GetPizzaType(int? id)
        {
            PizzaType pizzaType =  Database.PizzaTypes.GetEntityById(id);
            return new PizzaTypeDTO()
            {
                PizzaID = pizzaType.PizzaId,
                Diameter = pizzaType.Diameter,
                PizzaTypeID = pizzaType.PizzatypeId,
                Price = pizzaType.Price,
                Weight = pizzaType.Weight
            };
        }

        public IEnumerable<PizzaTypeDTO> GetPizzaTypes()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaType, PizzaTypeDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<PizzaType>, List<PizzaTypeDTO>>(Database.PizzaTypes.Get());
        }

        public IEnumerable<PizzaTypeDTO> GetPizzaTypesByPizzaId(int pizzaid)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PizzaType, PizzaTypeDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<PizzaType>, List<PizzaTypeDTO>>(Database.PizzaTypes.Get(t => t.PizzaId == pizzaid));
        }


        public int CreatePizzaType(PizzaTypeDTO pizzaTypeDto)
        {
            PizzaType pizzaType = new PizzaType()
            {
                Diameter = pizzaTypeDto.Diameter,
                Price = pizzaTypeDto.Price,
                Weight = pizzaTypeDto.Weight,
                PizzaId = pizzaTypeDto.PizzaID
            };
            Database.PizzaTypes.Insert(pizzaType);
            Database.Save();
            return pizzaType.PizzatypeId;

        }

        public string GetPizzaNameByPizzaTypeId(int? id)
        {
            if(id.HasValue)
                return Database.PizzaTypes.GetEntityById(id).Pizza.Name;
            return null;
        }
    }
}
