﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;

namespace MVCproject.BLL.Services
{
    public class UserService : IUserService
    {
        public IUnitOfWork Database { get; set; }

        public UserService(string connectionString = "DefaultConnection")
        {
            Database = new EfUnitOfWork(connectionString);
        }

        public UserService(IService service)
        {
            Database = service.Database;
        }

        public void CreateUser(UserDTO userDTO)
        { 
            Database.Users.Insert(new User()
            {
                Email = userDTO.Email,
                Login = userDTO.Login,
                Password = userDTO.Password,
                UserId = userDTO.UserId,
                Roles = Database.Roles.Get().Where(t => t.Name == "User").ToList()
            });

            Database.Save();
        }

        public UserDTO GetUser(string login)
        {
            IEnumerable<User> users = Database.Users.Get(t => t.Login == login);

            User user = null;
            List<RoleDTO> roleDTOs = null;

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Role, RoleDTO>()).CreateMapper();
            
            if (users.Count()>0)
            {
                user = users.SingleOrDefault();
                roleDTOs = mapper.Map<IEnumerable<Role>, List<RoleDTO>>(user.Roles);
            }
            
            return user == null ? null : new UserDTO
            {
                UserId = user.UserId,
                Login = user.Login,
                Password = user.Password,
                Email = user.Email,
                Roles = roleDTOs
            };
        }

        public UserDTO GetUser(int? id)
        {
            User user = Database.Users.GetEntityById(id);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Role, RoleDTO>()).CreateMapper();
            return new UserDTO()
            {
                Email = user.Email,
                Login = user.Login,
                Password = user.Password,
                UserId = user.UserId,
                Roles = mapper.Map<IEnumerable<Role>, List<RoleDTO>>(user.Roles)
            };
        }

        public List<UserDTO> GetUsers()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(Database.Users.Get());
        }

        public List<RoleDTO> GetRoles()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Role, RoleDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Role>, List<RoleDTO>>(Database.Roles.Get());
        }

        public void UpdateUser(UserDTO model)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RoleDTO, Role>()).CreateMapper();
            User user = new User
            {
                UserId = model.UserId,
                Login = model.Login,
                Email = model.Email,
                Password = model.Password,
                Orders = new List<Order>(),
                Roles = mapper.Map<IEnumerable<RoleDTO>, List<Role>>(model.Roles)
            };
            Database.Users.Update(user);
            Database.Save();
        }

        public void ChangeRoles(UserDTO model)
        {
            User user = Database.Users.Get(t => t.UserId == model.UserId).Single();

            user.Login = model.Login;
            user.Email = model.Email;
            user.Roles.Clear();
            Database.Users.Update(user);
            Database.Save();

            foreach (var role in model.Roles)
            {
                user.Roles.Add(Database.Roles.GetEntityById(role.RoleId));
            }

            Database.Save();
        }

        public void DeleteUser(int id)
        {
            Database.Users.Delete(id);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
