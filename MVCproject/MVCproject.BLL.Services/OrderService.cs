﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MVCproject.BLL.Interfaces;
using MVCproject.BLL.Models;
using MVCproject.BLL.Models.Cart;
using MVCproject.DAL.Entities.Models;
using MVCproject.DAL.Interfaces;
using MVCproject.DAL.Repositories;

namespace MVCproject.BLL.Services
{
    public class OrderService : IOrderService
    {
        public IUnitOfWork Database { get; set; }

        public OrderService(string connectionString = "DefaultConnection")
        {
            Database = new EfUnitOfWork(connectionString);
        }

        public OrderService(IService service)
        {
            Database = service.Database;
        }


        public void MakeOrder(IEnumerable<CartItem> cart, OrderDTO model)
        {
            var pizzatypeOrder = cart.Select(item => new PizzatypeOrderXref()
            {
                PizzatypeId = item.PizzaTypeDto.PizzaTypeID,
                Count = item.Amount
            }).ToList();

            var order = new Order
            {
                Date = DateTime.UtcNow,
                Name = model.Name,
                UserId = model.UserID,
                Adress = model.Adress,
                Phone = model.Phone,
                PizzatypeOrderXrefs = pizzatypeOrder
            };
            Database.Orders.Insert(order);
            Database.Save();
        }

        public List<OrderDTO> GetOrders()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Order>, List<OrderDTO>>(Database.Orders.Get());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
