﻿using System;
using System.Collections.Generic;
using NET02._1.Entities;

namespace NET02._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test test;
            bool ans;
            string[] names = new string[16]
            {
                "John Doe",
                "Doe, John",
                "Nick Brown",
                "Nick Brown",
                "John1 Doe1",
                "Doe2, John2",
                "Nick1 Brown1",
                "Nick Brown",
                "John3 Doe3",
                "Doe, John",
                "Nick Brown",
                "Nick3 Brown3",
                "John1 Doe1",
                "Doe23, John23",
                "Nick1 Brown1",
                "Nick Brown"
            };

            int[][] arrays = {
                                    new int[ ]{2, 7, 2, 8, 9 },
                                    new int[ ]{8, 3, 6, 8, 2 },
                                    new int[ ]{3, 6, 9, 1, 3 },
                                    new int[ ]{7, 2, 6, 4, 4 },
                                    new int[ ]{1, 3, 5, 7, 9 }
                                };


            int[] m = {1, 3, 5, 4, 2, 1, 3, 4, 2, 2, 3, 4, 2, 2, 5, 1};
            
            Statistics s = new Statistics();
            
            for (int i = 0; i < 16; i++)
            {
                int testNumber = m[i] - 1;
                test = new Test()
                {
                    TestName = "Testname " + (testNumber+1),
                    Date = DateTime.Now,
                    PersonName = "personname"
                };
                for (int j = 0; j < 5; j++)
                {
                    if (arrays[i/4][j] % 2 == 1)
                    {
                        ans = true;
                    }
                    else
                    {
                        ans = false;
                    }
                    test.Answers.Add(new Answer()
                    {
                        Answered = ans,
                        ComplexityCoefficient = arrays[testNumber][j],
                        QuestionText = "QuestionText - " + (testNumber + 1) + (j+1)
                    });
                }
                s.Add(names[i], test);
            }

            Console.WriteLine(s);

           


            Console.WriteLine("QuantityOfPassedTests:____________________________");
            foreach (KeyValuePair<string, int> var in s.QuantityOfPassedTests())
            {
                Console.WriteLine(var.Key + " - " + var.Value);
            }


            Console.WriteLine("MostPassableTest:_________________________________");
            Console.WriteLine(s.MostPassableTest());


            Console.WriteLine("PersonWithRightAnswers:___________________________");
            foreach (KeyValuePair<string, KeyValuePair<string, int>> personwithrightanswer in s.PersonWithRightAnswers())
            {
                Console.WriteLine("("+personwithrightanswer.Key+") - "  + personwithrightanswer.Value.Key + "("+ personwithrightanswer.Value.Value + ")");

            }

            Console.WriteLine("RarestAnsweredQuestions:__________________________");
            foreach (string question in s.RarestRightAnsweredQuestions())
            {
                Console.WriteLine(question);
            }

            Console.ReadKey();

        }
    }
}
