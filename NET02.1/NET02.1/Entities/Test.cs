﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1.Entities
{
    public class Test : IEnumerable<Answer>
    {
        private string testName;
        private string personName;


        public DateTime Date { get; set; }
        public List<Answer> Answers;

        public string TestName
        {
            get { return testName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("string mustn't be null or empty");
                }
                testName = value;   
            }
        }

        public string PersonName
        {
            get { return personName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("string mustn't be null or empty");
                }
                personName = value;
            }
        }

        
        public Test()
        {
            Answers = new List<Answer>();
        }


        public override bool Equals(object obj)
        {

            if (TestName == (obj as Test).TestName && this.PersonName == (obj as Test).PersonName)
            {
                for (int i = 0; i < Answers.Count; i++)
                {
                    if (Answers[i]!=(obj as Test).Answers[i])
                        return false;
                }

                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (TestName + PersonName + Date).GetHashCode();
        }


        public IEnumerator<Answer> GetEnumerator()
        {
            IEnumerable<Answer> answersIterator = Answers.OrderByDescending(answer => answer.ComplexityCoefficient);
            return (answersIterator).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
