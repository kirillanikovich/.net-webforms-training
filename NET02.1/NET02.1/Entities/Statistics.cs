﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NET02._1.Entities
{
    public class Statistics
    {
        private readonly Dictionary<string, List<Test>> _tests;

        public Statistics()
        {
            _tests = new Dictionary<string, List<Test>>();
        }

        public void Add(string personName, Test test=null)
        {
            personName = CheckPersonName(personName);
            test.PersonName = personName;
            if (!_tests.ContainsKey(personName))
            {
                _tests.Add(personName, new List<Test>());
            }

            _tests[personName].Add(test); //.Clone() as Test);
        }

        public List<Test> Get(string key)
        {
            return _tests[CheckPersonName(key)];
        }

        public List<Test> this[string key]
        {
            get
            {
                return Get(key);
            }
        }


        private string CheckPersonName(string personName)
        {
            if (personName.Contains(", "))
            {
                string[] conversedname = personName.Split(',', ' ');
                return conversedname[2] + " " + conversedname[0];
            }
            return personName;
        }

        public List<KeyValuePair<string, int>> QuantityOfPassedTests()
        {
            IEnumerable<KeyValuePair<string, int>> quantityOfEach = _tests.Select(pair => 
                                                    new KeyValuePair<string, int>(pair.Key, pair.Value.Count));
            return quantityOfEach.ToList(); 
        }

        public string MostPassableTest()
        {
            string passabletest = (from testlist in _tests.Values
                                from test in testlist
                                orderby test.TestName
                                group test by test.TestName
                                into groupedbynamestests
                                orderby groupedbynamestests.Count()
                                select groupedbynamestests.Key).Last();

            return passabletest;
        }

       
        public IEnumerable<KeyValuePair<string, KeyValuePair<string, int>>> PersonWithRightAnswers()
        {
            IEnumerable<KeyValuePair<string, KeyValuePair<string, int>>> peoplesRightAnswers =   from testlist in _tests.Values
                                        from test in testlist
                                        orderby test.TestName
                                        group test by test.TestName into groupedbynametests
                                        from valuestest in groupedbynametests
                                        select new KeyValuePair<string, KeyValuePair<string, int>>(valuestest.TestName,
                                        new KeyValuePair<string, int>(
                                            valuestest.PersonName,
                                            valuestest.Answers.Count(answer => answer.Answered)));

            return peoplesRightAnswers.ToList();
        }


        public List<String> RarestRightAnsweredQuestions()
        {
            IEnumerable<string> rarestRightAnsweredQuestions = 
                from groupedfailedquestions in (from testList in _tests.Values
                    from test in testList
                    from answer in test.Answers
                    group answer by answer.QuestionText into questions
                    orderby questions.Key
                    from answerToQuestion in questions
                    group answerToQuestion by questions.Count(t => t.Answered) into groupedQuestions
                    orderby groupedQuestions.Key
                    select groupedQuestions).First()
                group groupedfailedquestions by groupedfailedquestions.QuestionText into failedquestions
                select failedquestions.Key;

            return rarestRightAnsweredQuestions.ToList();
        }

        

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var test in _tests)
            {
                sb.AppendLine("Name: " + test.Key);
                foreach (var var in test.Value)
                {
                    sb.AppendLine("Testname: " + var.TestName + ", name:" + var.PersonName);
                    foreach (Answer answer in var.Answers)
                    {
                        sb.AppendLine(answer.ToString());
                    }
                }

                sb.AppendLine("================");
            }
            return sb.ToString();
        }
    }
}
