﻿
using System;

namespace NET02._1.Entities
{
    public class Answer
    {
        private string questionText;
        private double complexityCoefficient;

        public string QuestionText
        {
            get
            {
                return questionText;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("string mustn't be null or empty");
                }
                questionText = value;
            }
        }

        public double ComplexityCoefficient
        {
            get
            {
                return complexityCoefficient;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                complexityCoefficient = value;
            }
        }

        public bool Answered { get; set; }
        
        public override string ToString()
        {
            return questionText + ", C:" + complexityCoefficient + ", ->" + Answered;
        }
    }
}
