﻿using System;
using System.Collections.Generic;
using NET02._1.Entities;
using NUnit.Framework;


namespace NET02._1.Tests.Entities
{
    [TestFixture]
    public class StatisticsTests
    {
        private Statistics s;

        private Test CreateTestClass()
        {
            Test t = new Test()
            {
                Answers = new List<Answer>(),
                TestName = "Testname 6",
                Date = DateTime.Now,
                PersonName = "test"
            };


            t.Answers.Add(new Answer()
                {
                    Answered = false,
                    QuestionText = "QuestionText - 61",
                    ComplexityCoefficient = 2
                });
            t.Answers.Add(new Answer()
                {
                Answered = true,
                    QuestionText = "QuestionText - 62",
                    ComplexityCoefficient = 7
            });
            t.Answers.Add(new Answer()
                {
                Answered = false,
                    QuestionText = "QuestionText - 63",
                    ComplexityCoefficient = 2
            });
            t.Answers.Add(new Answer()
                {
                    Answered = false,
                    QuestionText = "QuestionText - 64",
                    ComplexityCoefficient = 8
            });
            t.Answers.Add(new Answer()
                {
                Answered = true,
                    QuestionText = "QuestionText - 65",
                    ComplexityCoefficient = 9
            });


            return t;
        }


        [SetUp]
        public void SetUp()
        {
            Test test;
            bool ans;

            string[] names = new string[16]
            {
                "John Doe",
                "Doe, John",
                "Nick Brown",
                "Nick Brown",
                "John1 Doe1",
                "Doe2, John2",
                "Nick1 Brown1",
                "Nick Brown",
                "John3 Doe3",
                "Doe, John",
                "Nick Brown",
                "Nick3 Brown3",
                "John1 Doe1",
                "Doe23, John23",
                "Nick1 Brown1",
                "Nick Brown"
            };

            int[][] arrays = {
                new [ ]{2, 7, 2, 8, 9 },
                new [ ]{8, 3, 6, 8, 2 },
                new [ ]{3, 6, 9, 1, 3 },
                new [ ]{7, 2, 6, 4, 4 },
                new [ ]{1, 3, 5, 7, 9 }
            };

            int[] m = { 1, 3, 5, 4, 2, 1, 3, 4, 2, 2, 3, 4, 2, 2, 5, 1 };

            s = new Statistics();
            
            for (int i = 0; i < 16; i++)
            {
                int testNumber = m[i] - 1;
                test = new Test()
                {
                    TestName = "Testname " + (testNumber + 1),
                    Date = DateTime.Now,
                    PersonName = "testname"
                };
                for (int j = 0; j < 5; j++)
                {
                    if (arrays[i/4][j] % 2 == 1)
                    {
                        ans = true;
                    }
                    else
                    {
                        ans = false;
                    }
                    test.Answers.Add(new Answer()
                    {
                        Answered = ans,
                        ComplexityCoefficient = arrays[testNumber][j],
                        QuestionText = "QuestionText - " + (testNumber + 1) + (j + 1)
                    });
                }
                s.Add(names[i], test);
            }
        }


        /// <summary>
        /// Check the methods:
        /// -Add
        /// -CheckPersonName
        /// -Get()
        /// -indexer(readonly)
        /// </summary>
        [Test]
        public void Indexer_AddElement_ReturnTrue()
        {
            Test t = CreateTestClass();

            s.Add("Gates, Bill", t);
            List<Test> testlist = new List<Test>();
            testlist = s["Bill Gates"];

            Assert.Contains(t, testlist);
        }


        [Test]
        public void MostPassableTest_Query_ReturnTestname2()
        {
            string mostPassableTest = s.MostPassableTest();
            Assert.AreEqual("Testname 2", mostPassableTest);
        }


        [Test]
        public void RarestRightAnsweredQuestions_Query_Return_13_14_53_54()
        {
            List<string> rarestRightAnsweredQuestions = s.RarestRightAnsweredQuestions();
            List<string> equalList = new List<string>
            {
                "QuestionText - 13",
                "QuestionText - 14",
                "QuestionText - 53",
                "QuestionText - 54"
            };

            for (int i = 0; i < rarestRightAnsweredQuestions.Count; i++)
            {
                Assert.AreEqual(rarestRightAnsweredQuestions[i], equalList[i]);
            }
        }

    }
}
