﻿using NET03._1WF.Sensors;
using System.Collections.Generic;

namespace NET03._1WF.Repository
{
    public interface IRepository
    {
        void Save(List<Sensor> sensors);
        List<Sensor> Read();
    }
}
