﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using NET03._1WF.Sensors;

namespace NET03._1WF.Repository
{
    public class XMLRepository : IRepository
    {
        public void Save(List<Sensor> sensors)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode docNode = xDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xDoc.AppendChild(docNode);
            XmlNode productsNode = xDoc.CreateElement("Sensors");
            xDoc.AppendChild(productsNode);

            foreach (Sensor s in sensors)
            {
                XmlElement sensorElement = xDoc.CreateElement("Sensor");

                XmlElement idElement = xDoc.CreateElement("Id");
                XmlElement typeElement = xDoc.CreateElement("Type");
                XmlElement intervalElement = xDoc.CreateElement("Interval");
                XmlElement valueElement = xDoc.CreateElement("Value");

                XmlText idText = xDoc.CreateTextNode(s.Id.ToString());
                XmlText typeText = xDoc.CreateTextNode(s.Type.ToString());
                XmlText intervalText = xDoc.CreateTextNode(s.Interval.ToString());
                XmlText valueText = xDoc.CreateTextNode(s.Value.ToString());
                
                idElement.AppendChild(idText);
                typeElement.AppendChild(typeText);
                intervalElement.AppendChild(intervalText);
                valueElement.AppendChild(valueText);

                sensorElement.AppendChild(idElement);
                sensorElement.AppendChild(typeElement);
                sensorElement.AppendChild(intervalElement);
                sensorElement.AppendChild(valueElement);

                productsNode.AppendChild(sensorElement);
            }
            string sourceSir = Environment.CurrentDirectory + "\\Sensors\\";
            if (!Directory.Exists(sourceSir))
            {
                Directory.CreateDirectory(sourceSir);
            }
            xDoc.Save(sourceSir + "Sensors.xml");
        }
        
        public List<Sensor> Read()
        {
            List<Sensor> Sensors = new List<Sensor>();
            Sensor sensor;

            string sourceSir = Environment.CurrentDirectory + "\\Sensors\\";
            if (!Directory.Exists(sourceSir))
            {
                return new List<Sensor>();
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(sourceSir + "Sensors.xml");

            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                sensor = new Sensor();
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    switch(childnode.Name)
                    {
                        case "Id":
                            {
                                sensor.Id = Int32.Parse(childnode.InnerText);
                            }
                        break;
                        case "Type":
                            {
                                if (childnode.InnerText == SensorType.TEMPTERATURE.ToString())
                                {
                                    sensor.Type = SensorType.TEMPTERATURE;
                                }
                                if (childnode.InnerText == SensorType.PRESSURE.ToString())
                                {
                                    sensor.Type = SensorType.PRESSURE;
                                }
                                if (childnode.InnerText == SensorType.HUMIDITY.ToString())
                                {
                                    sensor.Type = SensorType.HUMIDITY;
                                }
                            }
                        break;
                        case "Interval":
                            {
                                sensor.Interval = Int32.Parse(childnode.InnerText);
                            }
                        break;
                        case "Value":
                            {
                                sensor.Value = Int32.Parse(childnode.InnerText); 
                            }
                        break;
                    }
                }
                Sensors.Add(sensor);
            }
            return Sensors;
        }
    }
}
