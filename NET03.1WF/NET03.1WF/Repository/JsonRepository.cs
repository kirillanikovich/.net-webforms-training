﻿using System;
using System.Collections.Generic;
using System.IO;
using NET03._1WF.Sensors;
using Newtonsoft.Json;

namespace NET03._1WF.Repository
{
    public class JsonRepository : IRepository
    {
        private string _path = "";
        private string _sourceSir = "";

        public JsonRepository()
        {
            _sourceSir = Environment.CurrentDirectory + "\\Sensors\\";
            if (!Directory.Exists(_sourceSir))
            {
                Directory.CreateDirectory(_sourceSir);
            }
            _path = _sourceSir + "Sensors.json";
        }

        public void Save(List<Sensor> sensors)
        {
            File.WriteAllText(_path, JsonConvert.SerializeObject(sensors, Formatting.Indented));
        }

        public List<Sensor> Read()
        {
            string json = File.ReadAllText(_path);
            List<Sensor> sensors = JsonConvert.DeserializeObject<List<Sensor>>(json);
            return sensors;
        }
    }
}
