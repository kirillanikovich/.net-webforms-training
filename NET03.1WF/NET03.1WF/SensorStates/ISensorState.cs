﻿namespace NET03._1WF.Sensors
{
    public interface ISensorState
    { 
        void Next(Sensor sensor);
    }
}