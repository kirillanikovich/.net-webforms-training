﻿namespace NET03._1WF.Sensors
{
    public class CalibrationState : ISensorState
    {
        public void Next(Sensor sensor)
        {
            sensor.StopCalibrateSensor();
            sensor.StartMeasureValues();
        }
    }
}