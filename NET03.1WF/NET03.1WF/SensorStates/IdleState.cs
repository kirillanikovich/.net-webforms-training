﻿namespace NET03._1WF.Sensors
{
    public class IdleState : ISensorState
    {
        public void Next(Sensor sensor)
        {
            sensor.Value = 0;
            sensor.StartCalibrateSensor();
        }
    }
}