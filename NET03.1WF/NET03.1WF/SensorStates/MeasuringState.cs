﻿namespace NET03._1WF.Sensors
{
    public class MeasuringState : ISensorState
    {
        public void Next(Sensor sensor)
        {
            sensor.StopMeasureValues();
            sensor.Notify();
        }
    }
}