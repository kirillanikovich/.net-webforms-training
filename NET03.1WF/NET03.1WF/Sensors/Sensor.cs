﻿using System;
using System.Threading;

namespace NET03._1WF.Sensors
{
    public class Sensor : IDisposable, IObservable<string[]>
    {
        private int _value;
        private SensorId _sensorId { get; }
        private int _interval;
        private ISensorState _state = new IdleState();
        private IObserver<string[]> _sensorObserver { get; set; }
        private Timer _timer;
        public int Id { get; set; }
        public SensorType Type { get; set; }
        public int Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                _interval = value;
                if (_timer != null)
                {
                    if (_state.GetType().Equals(typeof(MeasuringState)))
                    {
                        StopMeasureValues();
                        StartMeasureValues();
                    }
                    if (_state.GetType().Equals(typeof(CalibrationState)))
                    {
                        StopCalibrateSensor();
                        StartCalibrateSensor();
                    }
                }
            }
        }
        public int Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    Notify();
                }
            }
        }

        public Sensor(SensorType type = SensorType.TEMPTERATURE, ISensorState state = null)
        {
            _sensorId = SensorId.GetInstance();
            Type = type;
            if (state != null)
            {
                _state = state;
            }
            else
            {
                _state = new IdleState();
            }
            Id = _sensorId.Id;
            Interval = new Random(Guid.NewGuid().GetHashCode()).Next(950, 1050);
        }

        public void Notify()
        {
            if (_sensorObserver!=null)
            {
                _sensorObserver.OnNext(new string[]
                {
                    Type.ToString(),
                    Id.ToString(),
                    _state.GetType().Name,
                    Value.ToString(),
                    Interval.ToString()
                });
            }
        }
                
        public void Next()
        {
            _state.Next(this);
        }

        public void Generate(object o = null)
        {
            Value = new Random(Guid.NewGuid().GetHashCode()).Next(1000, 2000);
        }

        public void StartMeasureValues()
        {
            if (_timer == null)
            {
                _timer = new Timer(Generate, null, 0, Interval);
            }
            _state = new MeasuringState();
        }

        public void StopMeasureValues()
        {
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
            _state = new IdleState();
        }

        public void StartCalibrateSensor()
        {
            if (_timer == null)
            {
                Value = 0;
                _timer = new Timer((arg) => Value++, null, 0, 1000);
            }
            _state = new CalibrationState();
        }

        public void StopCalibrateSensor()
        {
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
        }

        public IDisposable Subscribe(IObserver<string[]> observer)
        {
            _sensorObserver = observer;
            return (IDisposable)observer;
        }

        public void Dispose()
        {
            ((IDisposable)_sensorObserver).Dispose();
        }
    }
}
