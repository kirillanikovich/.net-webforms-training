﻿namespace NET03._1WF.Sensors
{
    public enum SensorType
    {
        TEMPTERATURE,
        PRESSURE,
        HUMIDITY
    }
}
