﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NET03._1WF.Sensors
{
    public class SensorViewItem : ListViewItem, IObserver<string[]>, IDisposable
    {
        private delegate void SetTextCallback(string[] parameters);
        
        public void OnNext(string[] parameters)
        {
            if (ListView!=null && ListView.InvokeRequired)
            {
                SetTextCallback d = OnNext;
                ListView.Invoke(d, new object[] { parameters });
            }
            else
            {
                if (SubItems.Count > 0)
                {
                    SubItems.Clear();
                    Name = Text = parameters[0];//_sensorType;
                }
                for(int i=1; i< parameters.Length; i++)
                {
                    SubItems.Add(parameters[i]);
                }
                if (Selected)
                {
                    ForeColor = Color.White;
                    BackColor = Color.DodgerBlue;
                }
                else
                {
                    ForeColor = Color.Black;
                    BackColor = Color.White;
                }
            }
        }
        
        public void OnError(Exception error) { }

        public void OnCompleted() { }

        public void Dispose() { }
    }
}
