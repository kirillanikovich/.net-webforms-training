﻿using System;

namespace NET03._1WF.Sensors
{
    public class SensorId
    {
        private static readonly SensorId _instance = new SensorId();
        private int _id;

        public int Id
        {
            get
            {
                return ++_id;
            }
        }

        protected SensorId()
        {
            _id = 0;
        }

        public static SensorId GetInstance()
        {
            return _instance;
        }
        public void Reset()
        {
            _id = 0;
        }
     }
}     

