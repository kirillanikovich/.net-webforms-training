﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using NET03._1WF.Repository;
using NET03._1WF.Sensors;

namespace NET03._1WF
{
    public partial class Form1 : Form
    {
        private RadioButton _selectedrb = null;
        public List<Sensor> Sensors;
        IRepository repository;

        public Form1()
        {
            InitializeComponent();
            radioButton2.CheckedChanged += RadioButton_CheckedChanged;
            radioButton1.CheckedChanged += RadioButton_CheckedChanged;
            RadioButton_CheckedChanged(radioButton1, null);
            Sensors = new List<Sensor>();
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            foreach (var sensor in Sensors)
            {
                sensor.Generate(null);
            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            AddSensor(_1WF.Sensors.SensorType.TEMPTERATURE);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            AddSensor(_1WF.Sensors.SensorType.PRESSURE);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            AddSensor(_1WF.Sensors.SensorType.HUMIDITY);
        }

        private void AddSensor(SensorType sensorType)
        {
            Sensor s = new Sensor(sensorType, new IdleState());
            SensorViewItem listItem = new SensorViewItem();
            s.Subscribe(listItem);
            s.Notify();
            Sensors.Add(s);
            listView1.Items.Add(listItem);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            switch(_selectedrb.Text){
                case "XML":
                    repository = new XMLRepository();
                    break;
                case "JSON":
                    repository = new JsonRepository();
                    break;
                default:
                    return;
            }
            repository.Save(Sensors);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (Sensors.Count>0)
            {
                Sensors.Clear();
                listView1.Items.Clear();
            }

            switch (_selectedrb.Text)
            {
                case "XML":
                    repository = new XMLRepository();
                    break;
                case "JSON":
                    repository = new JsonRepository();
                    break;
                default:
                    return;
            }

            SensorId.GetInstance().Reset();
            foreach(Sensor s in repository.Read())
            {
                SensorViewItem listItem = new SensorViewItem();
                s.Subscribe(listItem);
                Sensors.Add(s);
                listView1.Items.Add(listItem);
                s.Notify();
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices != null && listView1.SelectedIndices.Count>0)
            {
                Sensors[listView1.SelectedIndices[0]].Dispose();
                Sensors.RemoveAt(listView1.SelectedIndices[0]);
                listView1.Items[listView1.SelectedIndices[0]].Remove();
                listView1.SelectedIndices.Clear();
            }
        }

        void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb == null)
            {
                MessageBox.Show("Sender is not a RadioButton");
                return;
            }
            if (rb.Checked)
            {
                _selectedrb = rb;
            }
        }
        
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(ListViewItem aa in listView1.Items)
            {
                aa.BackColor = Color.White;
                aa.ForeColor = Color.Black;
            }
            if (listView1.SelectedIndices != null && listView1.SelectedIndices.Count > 0)
            {
                textBox1.Text = Sensors[listView1.SelectedIndices[0]].Interval.ToString();
            }
            else
            {
                textBox1.Text = "";
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices != null && listView1.SelectedIndices.Count > 0)
            {
                Sensors[listView1.SelectedIndices[0]].Interval = Int32.Parse(textBox1.Text);
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices != null && listView1.SelectedIndices.Count > 0)
            {
                Sensors[listView1.SelectedIndices[0]].Next();
            }
        }
    }
}
