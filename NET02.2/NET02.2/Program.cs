﻿using System;

namespace NET02._2
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigManager configManager = new ConfigManager();
            configManager.ParseXml("file.xml");
            Console.WriteLine(configManager.ShowConfig());
            Console.WriteLine("Logins where config are incorrect:");
            foreach (var name in configManager.IncorrectLoginNames())
            {
                Console.WriteLine(name);
            }
            configManager.NormalizeIncorrectLogins();
            Console.WriteLine(configManager.ShowConfig());
            Console.WriteLine("Logins where config are incorrect:");
            foreach (var name in configManager.IncorrectLoginNames())
            {
                Console.WriteLine(name);
            }

            Console.WriteLine("XML=>JSON\n" + configManager.DivideOnFilesJson());

            Console.ReadKey();
        }
    }
}
