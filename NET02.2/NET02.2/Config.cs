﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NET02._2
{
    public class Config:IEnumerable<Login>
    {
        public List<Login> ConfigList;

        public Config()
        {
            ConfigList = new List<Login>();
        }

        public void Add(Login login)
        {
            ConfigList.Add(login);
        }

        public IEnumerator<Login> GetEnumerator()
        {
            return ((IEnumerable<Login>)ConfigList).GetEnumerator();
        }

        public List<Login> IncorrectLogins()
        {
            return new List<Login>(
                ConfigList.Where(t => !t.IsCorrect())
                );
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Login login in ConfigList)
            {
                sb.Append(login);
            }
            return sb.ToString();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Login>)ConfigList).GetEnumerator();
        }
    }
}
