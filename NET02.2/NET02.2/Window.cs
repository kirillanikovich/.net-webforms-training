﻿using System.Text;

namespace NET02._2
{
    public class Window
    {
        public int? Top;
        public int? Left;
        public int? Width;
        public int? Height;
        public string Title;

        public Window(string title=null)
        {
            Top = Left = Width = Height = null;
            Title = title;
        }

        public bool IsCorrect()
        {
            return (
                    Height != null &&
                    Top != null &&
                    Left != null &&
                    Width != null);
        }

        public void NormalizeIncorrectWindow()
        {
            if (Height == null)
            {
                Height = 150;
            }
            if (Width == null)
            {
                Width = 400;
            }
            if (Top == null)
            {
                Top = 0;
            }
            if (Left == null)
            {
                Left = 0;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("  " + Title + "(" + (Top != null ? Top.ToString() : "-"));
            sb.Append(", " + (Left != null ? Left.ToString() : "-"));
            sb.Append(", " + (Width != null ? Width.ToString() : "-"));
            sb.Append(", " + (Height != null ? Height.ToString() : "-") + ")");
            return sb.ToString();
        }


    }
}
