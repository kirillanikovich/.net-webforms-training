﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace NET02._2
{
    public class ConfigManager
    {
        private XmlDocument xDoc;
        private XmlElement xRoot;
        private Window defaultWindow;
        private Config config;

        public ConfigManager()
        {
            xDoc = new XmlDocument();
        }

        /// <summary>
        /// Method parses XML file uses standards-based support for xml processing.
        /// </summary>
        public void ParseXml(string path)
        {
            xDoc.Load("file.xml");
            xRoot = xDoc.DocumentElement;
            config = new Config();

            foreach (XmlNode loginNode in xRoot)
            {
                XmlNode attr = loginNode.Attributes.GetNamedItem("name");
                if (attr != null)
                {
                    Login login = new Login
                    {
                        Name = attr.Value,
                        Windows = new List<Window>()
                    };

                    foreach (XmlNode window in loginNode.ChildNodes)
                    {
                        Window windowObject = new Window();
                        foreach (XmlNode xmlparams in window)
                        {
                            int value;
                            if (Int32.TryParse(xmlparams.InnerText, out value))
                            {
                                switch (xmlparams.Name)
                                {
                                    case "top":
                                        windowObject.Top = value;
                                        break;
                                    case "left":
                                        windowObject.Left = value;
                                        break;
                                    case "width":
                                        windowObject.Width = value;
                                        break;
                                    case "height":
                                        windowObject.Height = value;
                                        break;
                                }
                            }
                        }
                        windowObject.Title = window.Attributes.GetNamedItem("title").Value;
                        login.Windows.Add(windowObject);
                    }
                    config.Add(login);
                }
            }
        }

        public IEnumerable<string> IncorrectLoginNames()
        {
            return config.IncorrectLogins().Select(t => t.Name);
        }

        public void NormalizeIncorrectLogins()
        {
            if (config != null)
            {
                foreach (Login login in config.IncorrectLogins())
                {
                    login.NormalizeIncorrectLogin();
                }
            }
        }

        public string ShowConfig()
        {
            return config.ToString();
        }
        
        /// <summary>
        /// Method divides loaded in constructor file to files
        /// on separated XML files in which the field of login and fields of windows contains. Files are stored in the directory "Config/*.xml"
        /// </summary>
        public string DivideOnFilesJson(string _path="")
        {
            StringBuilder report = new StringBuilder();
            if (config == null)
            {
                ParseXml(_path);
            }

            foreach (Login login in config)
            {
                string source_dir = Environment.CurrentDirectory + "\\Config\\";
                if (!System.IO.Directory.Exists(source_dir))
                {
                    System.IO.Directory.CreateDirectory(source_dir);
                }
                string path = source_dir + login.Name + ".json";
                File.WriteAllText(path, JsonConvert.SerializeObject(login, Newtonsoft.Json.Formatting.Indented));
                report.Append(JsonConvert.SerializeObject(login, Newtonsoft.Json.Formatting.Indented));
            }
            return report.ToString();
        }
    }
}

