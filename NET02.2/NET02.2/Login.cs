﻿using System.Collections.Generic;
using System.Text;

namespace NET02._2
{
    public class Login
    {
        public string Name;
        public List<Window> Windows;


        public bool IsCorrect()
        {
            bool correctness = true;
            foreach (Window window in Windows)
            {
                if (window.Title == "main" && !window.IsCorrect())
                {
                    correctness = false;
                }
            }
            return correctness;
        }

        public void NormalizeIncorrectLogin()
        {
            foreach (Window window in Windows)
            {
                if (window.Title == "main")
                {
                    window.NormalizeIncorrectWindow();
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Login: " + Name);
            foreach (Window window in Windows)
            {
                sb.AppendLine(window.ToString());
            }

            return sb.ToString();
        }
    }
}
