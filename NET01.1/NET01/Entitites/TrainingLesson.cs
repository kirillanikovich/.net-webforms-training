﻿using System;
using NET01.Entitites;
using NET01.Entitites.Enums;

namespace NET01
{
    public class TrainingLesson : TrainingEntity, IVersionable, ICloneable
    {
        private static readonly byte VersionMassiveSize = 8;
        private static readonly byte TrainingMaterialCount = 20;
        private readonly byte[] _version;
        private int _countOfTrainingMaterials;
        private TrainingMaterial[] _listOfMaterials;

        public string LessonName { get; set; }

        public TrainingLesson(string name, byte[] version, string description = null) : base(description)
        {
            _countOfTrainingMaterials = 0;
            _listOfMaterials = new TrainingMaterial[TrainingMaterialCount];
            LessonName = name;
            _version = new byte[VersionMassiveSize];
            Array.Copy(version, _version, VersionMassiveSize);
        }
        
        public object Clone()
        {
            var clonedLesson = new TrainingLesson(LessonName, _version, Description)
            {
                Guid = Guid,
                LessonName = LessonName,
                Description = Description,
                _countOfTrainingMaterials = _countOfTrainingMaterials,
                _listOfMaterials = new TrainingMaterial[TrainingMaterialCount]
            };
            for (var i = 0; i < _countOfTrainingMaterials; i++)
            {
                clonedLesson._listOfMaterials[i] = _listOfMaterials[i].Clone();
            }

            return clonedLesson;
        }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(byte[] sourceBytes)
        {
            Array.Copy(sourceBytes, _version, VersionMassiveSize);
        }

        public string GetVersionString()
        {
            var ver = "";
            foreach (var n in _version) ver += n;
            return ver;
        }


        public void AddTrainingMaterial(TrainingMaterial trainingMaterial)
        {
            _listOfMaterials[_countOfTrainingMaterials] = trainingMaterial;
            _countOfTrainingMaterials++;
            Console.WriteLine("To lesson '" + LessonName + "' added material " + trainingMaterial.GetType().Name +
                              ", description: " + trainingMaterial);
        }

        public string GetLessonType()
        {
            var typeOfLesson = LessonType.TextLesson;
            foreach (var material in _listOfMaterials)
                if (material is VideoMaterial)
                    typeOfLesson = LessonType.VideoLesson;
            return typeOfLesson.ToString();
        }
        
        public override string Show()
        {
            var s = base.Show() +
                    "Lesson name: " + LessonName + Environment.NewLine +
                    "Version: " + GetVersionString() + Environment.NewLine +
                    "Count of training materials: " + _countOfTrainingMaterials + Environment.NewLine;

            for (var i = 0; i < _countOfTrainingMaterials; i++)
            {
                s += "#######" + Environment.NewLine;
                s += _listOfMaterials[i].Show();
            }

            return s;
        }
    }
}