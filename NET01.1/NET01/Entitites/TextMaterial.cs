﻿using System;

namespace NET01.Entitites
{
    public class TextMaterial : TrainingMaterial
    {
        private string _textofMaterial;
        public string TextOfMaterial
        {
            get => _textofMaterial;
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException();
                }
                if (value.Length > 10000)
                {
                    throw new ArgumentException("Length of Text(string) must me 0-10000");
                }
                _textofMaterial = value;
            }
        }

        public TextMaterial(string text = null, string description = null) : base(description)
        {
            TextOfMaterial = text;
        }

        public override string Show()
        {
            return base.Show() +
                   "Text of the material: " + TextOfMaterial + Environment.NewLine;
        }

        public override TrainingMaterial Clone()
        {
            return new TextMaterial(TextOfMaterial, Description) {Guid = Guid};
        }
    }
}