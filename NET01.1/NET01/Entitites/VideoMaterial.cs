﻿using System;
using NET01.Entitites.Enums;

namespace NET01.Entitites
{
    public class VideoMaterial : TrainingMaterial, IVersionable
    {
        private static readonly byte VersionMassiveSize = 8;
        private readonly byte[] _version;
        public VideoFormat FormatVideo { get; set; }
        public Uri VideoUri { get; set; }
        public Uri ImagePreviewUri { get; set; }


        public VideoMaterial(string videoLink, byte[] version, string description = null, string previewLink = "",
            VideoFormat videoformat = VideoFormat.Unknown) : base(description)
        {
            VideoUri = new Uri(videoLink);
            ImagePreviewUri = new Uri(previewLink);
            FormatVideo = videoformat;
            _version = new byte[VersionMassiveSize];
            Array.Copy(version, _version, VersionMassiveSize);
        }


        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(byte[] sourceBytes)
        {
            Array.Copy(sourceBytes, _version, VersionMassiveSize);
        }

        public string GetVersionString()
        {
            var ver = "";
            foreach (var n in _version) ver += n;
            return ver;
        }

        public override string Show()
        {
            return base.Show() +
                   "VideoUri: " + VideoUri.AbsoluteUri + Environment.NewLine +
                   "ImagePreviewUri: " + ImagePreviewUri.AbsoluteUri + Environment.NewLine +
                   "VideoFormat: " + FormatVideo + Environment.NewLine +
                   "Version: " + GetVersionString() + Environment.NewLine;
        }

        public override TrainingMaterial Clone()
        {
            return new VideoMaterial(VideoUri.AbsoluteUri, _version, Description, ImagePreviewUri.AbsoluteUri,
                FormatVideo) {Guid = Guid};
        }
    }
}