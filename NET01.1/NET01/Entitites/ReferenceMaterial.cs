﻿using System;
using NET01.Entitites.Enums;

namespace NET01.Entitites
{
    public class ReferenceMaterial : TrainingMaterial
    {
        public Uri Uri { get; set; }
        public LinkType LinkType { get; set; }

        public ReferenceMaterial(string link, LinkType linkType = LinkType.Unknown, string description = "") : base(
            description)
        {
            LinkType = linkType;
            Uri = new Uri(link);
        }

        public override string Show()
        {
            return base.Show() +
                   "Uri: " + Uri.AbsoluteUri + Environment.NewLine +
                   "Link Type: " + LinkType + Environment.NewLine;
        }

        public override TrainingMaterial Clone()
        {
            return new ReferenceMaterial(Uri.AbsoluteUri, LinkType, Description) { Guid = Guid };
        }
    }
}