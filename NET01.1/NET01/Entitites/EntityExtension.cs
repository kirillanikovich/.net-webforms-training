﻿using System;

namespace NET01.Entitites
{
    public static class EntityExtension
    {
        public static void AddGuid(this TrainingEntity trainingEntity)
        {
            trainingEntity.Guid = Guid.NewGuid();
        }
    }
}