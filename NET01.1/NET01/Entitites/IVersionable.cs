﻿namespace NET01.Entitites
{
    public interface IVersionable
    {
        byte[] GetVersion();
        void SetVersion(byte[] sourceBytes);
        string GetVersionString();
    }
}