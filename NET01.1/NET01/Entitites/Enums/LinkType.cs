﻿namespace NET01.Entitites.Enums
{
    public enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}