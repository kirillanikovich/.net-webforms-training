﻿namespace NET01.Entitites.Enums
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}