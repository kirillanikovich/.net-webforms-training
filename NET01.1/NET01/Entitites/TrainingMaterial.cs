﻿

namespace NET01.Entitites
{
    public abstract class TrainingMaterial : TrainingEntity
    {
        public TrainingMaterial(string description) : base(description){}

        public abstract TrainingMaterial Clone();
    }
}