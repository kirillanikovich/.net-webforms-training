﻿using System;

namespace NET01.Entitites
{
    public abstract class TrainingEntity
    {
        private string _description;
        public string Description
        {
            get => _description;

            set
            {
                if (value!=null && value.Length > 256)
                {
                    throw new ArgumentException("Length of description text must me 0-256");
                }
                _description = value;
            }
        }
        public Guid Guid { get; set; }
        

        protected TrainingEntity(string desc = null)
        {
            Guid = Guid.NewGuid();
            Description = desc;
        }

        public override string ToString()
        {
            return _description;
        }

        public virtual string Show()
        {
            return "Guid: " + Guid + Environment.NewLine +
                   "Desription: " + Description + Environment.NewLine;
        }

        public override bool Equals(object obj)
        {
            return obj is TrainingEntity && Guid == (obj as TrainingEntity).Guid;
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
    }
}