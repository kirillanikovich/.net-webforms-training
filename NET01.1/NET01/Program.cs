﻿using System;
using NET01.Entitites;
using NET01.Entitites.Enums;

namespace NET01
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var tl = new TrainingLesson("LESSON 1", new byte[] {1, 4, 6, 2, 7, 8, 2, 2});
            tl.AddTrainingMaterial(new VideoMaterial("https://youtube.com/video1", new byte[] {2, 4, 6, 2, 8, 9, 6, 1},
                "Desription of video No1", "http://image", VideoFormat.Mp4));
            tl.AddTrainingMaterial(new ReferenceMaterial("https://msdn.com", LinkType.Html));
            tl.AddTrainingMaterial(new TextMaterial("text of the material"));

            Console.WriteLine("Lesson type: " + tl.GetLessonType());
            Console.WriteLine(tl.Show());

            var tl2 = (TrainingLesson) tl.Clone();
            Console.WriteLine(tl2.Show());

            Console.WriteLine(tl.Equals(tl2));
            tl2.AddGuid();
            Console.WriteLine("tl2 new guid: " + tl2.Guid);
            Console.Read();
        }
    }
}