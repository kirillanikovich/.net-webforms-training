﻿using System;

namespace NET01._2.Matrix
{
    public class DiagMatrix<T> : Matrix<T>
    {
        /// <summary>
        /// Constructor of Matrix class. Creates a one-dimensional array of all elements of the diagonal matrix (n).
        /// </summary>
        /// <param name="dimension">This parameter uses for installing of matrix size. Cannot be less than 1. </param>

        public DiagMatrix(int dimension):base(dimension)
        {
        }

        /// <summary>
        /// The method creates the necessary size an array of the size "dimension".
        /// The method is called if the value "dimension" changes, then an array with a new dimension is recreated, the contents of the array are transferred.
        /// </summary>
        /// <param name="newDimension">This parameter uses for installing of matrix size. Cannot be less than 1. </param>
        protected override T[] CreateArray(int newDimension)
        {
            T[] newArray = new T[newDimension];
            if (newDimension < _dimension)
            {
                for (int i = 0; i < newDimension; i++)
                {
                    newArray[i] = Array[i];
                }
            }
            else
            {
                for (int i = 0; i < _dimension; i++)
                {
                    newArray[i] = Array[i];
                }
            }
            return newArray;
        }

        /// <returns>
        /// The method is used to check the correctness of the parameters, namely whether the parameters of rows and columns fall within a valid range.
        /// Return true value, if parametrs are correct and are in the admissible range of the dimension of the matrix
        /// </returns>
        protected override bool IndexRange(int rows, int cols)
        {
            return rows < 0 || cols < 0 || rows >= Dimension || cols >= Dimension || rows != cols;
        }

        /// <summary>
        /// Method declared for setting value in massive using the parameters of a two-dimensional array
        /// <param name="rows">Number of row</param>
        /// <param name="cols">Number of col</param>
        /// <param name="value">A value whose type is an array(Generic type)</param>
        /// </summary>
        protected override void SetArrayElement(int rows, int cols, T value)
        {
            Array[cols] = value;
        }

        /// <summary>
        /// Method declared for getting value in massive using the parameters of a two-dimensional array
        /// </summary>
        /// <param name="rows">Number of row</param>
        /// <param name="cols">Number of col</param>
        /// <param name="value">A value whose type is an array(Generic type)</param>
        protected override T GetArrayElement(int rows, int cols)
        {
            return Array[cols];
        }

        public override string ToString()
        {
            string s = "";
            for (var i = 0; i < Dimension; i++)
            {
                s += '|';
                for (var j = 0; j < Dimension; j++)
                {
                    {
                        if (i == j)
                        {
                            s = s + Array[j] + '|';
                        }
                        else
                        {
                            s = s + '-' + '|';
                        }
                    }
                }

                s += Environment.NewLine;
            }
            return s;
        }
    }
}