﻿using System;

namespace NET01._2.Matrix
{
    /// <summary>
    /// This class is a matrix represented as a one-dimensional array using a
    /// generic type as an array element for storing values.
    /// </summary>
    public class Matrix<T>
    {
        /// <summary>
        /// This event is called when a new matrix element value is set
        /// </summary>
        public event EventHandler<MatrixEventArgs<T>> Edited;

        /// <value>Store the dimension(size) of square matrix.</value>
        protected int _dimension;

        ///<typeparam name = "T" >
        ///This type parameter is used to store the value in the matrix. The matrix is in turn represented as a one-dimensional array.
        ///</typeparam >
        protected T[] Array;

        ///<property>
        /// This property is used to set the dimension of the matrix.
        /// Method Set throws ArgumentOutOfRangeException if dimension less than 1.
        /// </property>
        public int Dimension
        {
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException();
                }
                if (value>0 && value !=_dimension)
                {
                    Array = CreateArray(value);
                    _dimension = value;
                }

            }
            get => _dimension;
        }
        

        /// <summary>
        /// Constructor of Matrix class. Creates a one-dimensional array with the size of all elements of the matrix (n^2).
        /// </summary>
        /// <param name="dimension">This parameter uses for installing of matrix size. Cannot be less than 1. </param>
        public Matrix(int dimension)
        {
            _dimension = 0;
            if (dimension > 0)
            {
                Array = CreateArray(dimension);
                _dimension = dimension;
            }
        }

        /// <summary>
        /// The method creates the necessary size an array of the size "dimension".
        /// The method is called if the value "dimension" changes, then an array with a new dimension is recreated, the contents of the array are transferred.
        /// </summary>
        /// <param name="newDimension">This parameter uses for installing of matrix size. Cannot be less than 1. </param>
        protected virtual T[] CreateArray(int newDimension)
        {
            T[] newArray = new T[newDimension * newDimension];
            if (newDimension < _dimension)
            {
                for (int i = 0; i < newDimension; i++)
                {
                    for (int j = 0; j < newDimension; j++)
                    {
                        newArray[j + i * newDimension] = Array[j + i * newDimension];
                    }
                }
            }
            else
            {
                for (int i = 0; i < _dimension; i++)
                {
                    for (int j = 0; j < _dimension; j++)
                    {
                        newArray[j + i * newDimension] = Array[j + i * _dimension];
                    }
                }
            }
            return newArray;
        }


        /// <summary>
        /// Declared method provide indexer to setting and getting the matrix element, as a parameters for which the row and column values are used
        /// </summary>
        /// <param name="rows">Number of row</param>
        /// <param name="cols">Number of col</param>
        public T this[int rows, int cols]
        {
            get
            {
                if (IndexRange(rows, cols))
                {
                    throw new IndexOutOfRangeException();
                }
                return GetArrayElement(rows, cols);
            }

            set
            {
                if (IndexRange(rows, cols))
                {
                    throw new IndexOutOfRangeException();
                }

                if (!GetArrayElement(rows, cols).Equals(value))
                {
                    SetArrayElement(rows, cols, value);
                    OnEdited(new MatrixEventArgs<T>(value, rows, cols));
                }
            }
        }

        /// <returns>The method is used to check the correctness of the parameters, namely whether the parameters of rows and columns fall within a valid range.
        /// Return true value, if parametrs are correct and are in the admissible range of the dimension of the matrix </returns>
        protected virtual bool IndexRange(int rows, int cols)
        {
            return rows < 0 || cols < 0 || rows >= _dimension || cols >= _dimension;
        }

        /// <summary>
        /// Method declared for setting value in massive using the parameters of a two-dimensional array
        /// <param name="rows">Number of row</param>
        /// <param name="cols">Number of col</param>
        /// <param name="value">A value whose type is an array(Generic type)</param>
        /// </summary>
        protected virtual void SetArrayElement(int rows, int cols, T value)
        {
            Array[cols + rows * _dimension] = value;
        }

        /// <summary>
        /// Method declared for getting value in massive using the parameters of a two-dimensional array
        /// </summary>
        /// <param name="rows">Number of row</param>
        /// <param name="cols">Number of col</param>
        /// <param name="value">A value whose type is an array(Generic type)</param>
        protected virtual T GetArrayElement(int rows, int cols)
        {
            return Array[cols + rows * _dimension];
        }

        public override string ToString()
        {
            string s = "";
            for (var i = 0; i < _dimension; i++)
            {
                s += '|';
                for (var j = 0; j < _dimension; j++)
                {
                    s = s + Array[j + i * _dimension] + '|';
                }

                s += Environment.NewLine;
            }
            return s;
         }


        protected virtual void OnEdited(MatrixEventArgs<T> e)
        {
            EventHandler<MatrixEventArgs<T>> local = Edited;
            if (local != null)
            {
                local(this, e);
            }
        }

    }
}