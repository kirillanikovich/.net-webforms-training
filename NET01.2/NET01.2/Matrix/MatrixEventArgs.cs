﻿using System;

namespace NET01._2.Matrix
{
    /// <summary>
    /// This structure is a set of fields for passing the parameters of events of the Matrix class.
    /// </summary>
    /// <typeparam name="T">
    /// This type parameter is used to store the value in the matrix. The matrix is in turn represented as a one-dimensional array.
    /// This type must match the type that is used when instantiated the Matrix object.
    /// </typeparam>
    public class MatrixEventArgs<T> : EventArgs
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public T Value { get; set; }

        /// <param name="value">Value of new parameter</param>
        /// <param name="row">Number of the row</param>
        /// <param name="col">Number of the col</param>
        public MatrixEventArgs(T value, int row, int col)
        {
            Value = value;
            Row = row;
            Col = col;
        }
    }

}