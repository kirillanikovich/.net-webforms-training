﻿using System;
using NET01._2.Matrix;

namespace NET01._2
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Matrix <int> m = new Matrix<int>(3);

            m.Edited += delegate (object sender, MatrixEventArgs<int> arg)
            {
                Console.WriteLine("Matrix edited (anonymous method). Row = " + arg.Row + ", Col = " + arg.Col + ", value = '" + arg.Value + "'.");
            };
            
            Console.WriteLine(m);
            m[0, 0] = 1;
            m[0, 1] = 2;
            m[0, 2] = 3;
            m[1, 0] = 4;
            m[1, 1] = 5;
            m[1, 2] = 6;
            m[2, 0] = 7;
            m[2, 1] = 8;
            m[2, 2] = 9;
            m[1, 1] = 5;
            Console.WriteLine(m);

            m.Dimension = 2;
            Console.WriteLine(m);
            m.Dimension = 4;
            Console.WriteLine(m);

            DiagMatrix<int> m1 = new DiagMatrix<int>(3);

            m1.Edited += ConsoleMessage;
            m1[0, 0] = 1;
            m1[1, 1] = 5;
            m1[2, 2] = 9;
            Console.WriteLine(m1);


            DiagMatrix<int> m2 = new DiagMatrix<int>(5);
            m2.Edited += (sender, arg) =>
            {
                Console.WriteLine("Matrix edited (lambda-expression). Row = " + arg.Row + ", Col = " + arg.Col + ", value = '" + arg.Value + "'.");
            };

            m2[0, 0] = 1;
            m2[1, 1] = 3;
            m2[2, 2] = 5;
            m2[3, 3] = 7;
            m2[4, 4] = 9;
            Console.WriteLine(m2);
            m2.Dimension = 4;
            Console.WriteLine(m2);
            m2.Dimension = 6;
            Console.WriteLine(m2);

            Console.ReadKey();
        }

        private static void ConsoleMessage(object sender, MatrixEventArgs<int> arg)
        {
            Console.WriteLine("Matrix edited (method). Row = " + arg.Row + ", Col = " + arg.Col + ", value = '" + arg.Value + "'.");
        }
    }

}
