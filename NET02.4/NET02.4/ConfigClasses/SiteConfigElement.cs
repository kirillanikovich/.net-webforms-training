﻿using System;
using System.Configuration;

namespace NET02._4
{
    public class SiteConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("Name")]
        public string Name
        {
            get
            {
                return base["Name"] as string;
            }
        }

        [ConfigurationProperty("Timeout")]
        public int Timeout
        {
            get
            {
                return Int32.Parse(base["Timeout"].ToString());
            }
        }

        [ConfigurationProperty("ReplayPing")]
        public int ReplayPing
        {
            get
            {
                return Int32.Parse(base["ReplayPing"].ToString());
            }
        }


        [ConfigurationProperty("Url")]
        public string Url
        {
            get
            {
                return base["Url"] as string;
            }
        }

        [ConfigurationProperty("MailSettings")]
        public MailSettingsElement MailSettings
        {
            get { return base["MailSettings"] as MailSettingsElement; }
        }
    }
}