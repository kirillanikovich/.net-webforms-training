﻿using System.Configuration;

namespace NET02._4
{
    [ConfigurationCollection(typeof(SiteConfigElement), AddItemName = "Site")]
    public class SiteConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteConfigElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteConfigElement)element).Name;
        }
    }
}