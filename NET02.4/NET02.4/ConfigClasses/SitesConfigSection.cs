﻿using System;
using System.Configuration;

namespace NET02._4
{
    public class SitesConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Sites")]
        public SiteConfigCollection Sites
        {
            get { return base["Sites"] as SiteConfigCollection; }
        }
    }


    public class MailSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("host")]
        public string host
        {
            get { return base["host"] as string; }
        }


        [ConfigurationProperty("port")]
        public int port
        {
            get
            {

                return Int32.Parse(base["port"].ToString());
            }
        }


        [ConfigurationProperty("sender")]
        public string sender
        {
            get { return base["sender"] as string; }
        }

        
        [ConfigurationProperty("reciever")]
        public string reciever
        {
            get { return base["reciever"] as string; }
        }


        [ConfigurationProperty("enableSsl")]
        public bool enableSsl
        {
            get { return Boolean.Parse(base["enableSsl"].ToString()); }

        }


        [ConfigurationProperty("user")]
        public string user
        {
            get { return base["user"] as string; }
        }

        
        [ConfigurationProperty("password")]
        public string password
        {
            get { return base["password"] as string; }
        }


    }






}


//a.	Интервал между проверками (секунды);
//b.	Максимальное время ответа сервера (секунды);
//c.	Адрес проверяемой страницы;
//d.	Настройки почты администратора сайта;
