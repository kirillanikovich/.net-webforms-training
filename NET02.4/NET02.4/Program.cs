﻿using System;
using System.Diagnostics;
using System.Threading;

namespace NET02._4
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (Mutex mutex = new Mutex(false, "NET02.4"))
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(1), false))
                {
                    Console.WriteLine("Another instance is running");
                    return;
                }
               RunProgram();
            }
        }
        private static void RunProgram()
        {
            SitesMonitor siteMonitor = new SitesMonitor("NET02.4.exe");
            siteMonitor.StartThreads();
            Console.ReadKey();
        }
    }
}
