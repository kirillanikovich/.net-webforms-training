﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using NLog;

namespace NET02._4
{
    public class SitesMonitor
    {
        private const string Data = "this is a data string 32 bytes..";
        private static readonly PingOptions Options = new PingOptions(53, true);
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly byte[] _buffer = Encoding.ASCII.GetBytes(Data);
        private readonly string _exeName;
        private Configuration _configuration;
        private int[] _fails;
        private SitesConfigSection _sitesConfig;
        private List<Timer> _timers;

        public SitesMonitor(string exeName)
        {
            _exeName = exeName;
            ConfigFileMonitor();
            UploadConfig();
        }

        public void StartThreads()
        {
            _timers = new List<Timer>();
            _fails = new int[_sitesConfig.Sites.Count];
            for (int i = 0; i < _sitesConfig.Sites.Count; i++)
            {
                _timers.Add(new Timer(StartPingSite, i, 0, _sitesConfig.Sites.Cast<SiteConfigElement>().ToList()[i].ReplayPing));
            }
        }

        public void StartPingSite(object obj)
        {
            StringBuilder report = new StringBuilder();
            int index = (int) obj;
            SiteConfigElement site = _sitesConfig.Sites.Cast<SiteConfigElement>().ToList()[index];
            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send(site.Url, site.Timeout, _buffer, Options);
            if (reply != null && reply.Status != IPStatus.Success)
            {
                report.Append("\n            " + ++_fails[index]);
            }
            else
            {
                _fails[index] = 0;
            }
            report.Append("\n###########   " + site.Url + Environment.NewLine 
                          + "REPLAY PING   " + site.ReplayPing + Environment.NewLine + 
                          DisplayReply(reply));
            Logger.Info(report.ToString);
            pingSender.Dispose();
            if (_fails[index] > 2)
            {
                SendNotification(site);
                _timers[index].Dispose();
            }
        }

        private void UploadConfig()
        {
            _configuration = ConfigurationManager.OpenExeConfiguration(_exeName);
            _sitesConfig = (SitesConfigSection) _configuration.GetSection("SitesConfig");
        }


        public void ConfigFileMonitor()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = Directory.GetCurrentDirectory();
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName |
                                   NotifyFilters.DirectoryName;
            watcher.Filter = "*.exe.config";
            watcher.Changed += OnConfigChanged;
            watcher.Created += OnConfigChanged;
            watcher.Deleted += OnConfigChanged;
            watcher.EnableRaisingEvents = true;
        }

        private void OnConfigChanged(object source, FileSystemEventArgs e)
        {
            UploadConfig();
            for (int i = 0; i < _sitesConfig.Sites.Count; i++)
            {
                try
                {
                    _timers[i].Change(0, _sitesConfig.Sites.Cast<SiteConfigElement>().ToList()[i].ReplayPing);
                }
                catch (ObjectDisposedException)
                {
                }
            }
        }


        public string DisplayReply(PingReply reply)
        {
            if (reply == null)
            {
                return "NULL";
            }
            string replyText = "Ping status:  " + reply.Status + Environment.NewLine + "              " + reply.Address + Environment.NewLine;
            if (reply.Status == IPStatus.Success)
            {
                replyText += "RTT: " + reply.RoundtripTime + ", TTL: " + reply.Options.Ttl + Environment.NewLine;
            }
            return replyText += "============================================\n";
        }


        public async void SendNotification(SiteConfigElement site)
        {
            MailMessage message = new MailMessage();
            MailAddress sender = new MailAddress(site.MailSettings.sender);
            MailAddress receiver = new MailAddress(site.MailSettings.reciever);
            SmtpClient smtp = new SmtpClient
            {
                Host = site.MailSettings.host,
                Port = site.MailSettings.port,
                EnableSsl = site.MailSettings.enableSsl,
                Credentials = new NetworkCredential(site.MailSettings.user, site.MailSettings.password)
            };
            message.From = sender;
            message.To.Add(receiver);
            message.Body = site.Name + " - not working.";
            message.IsBodyHtml = true;
            smtp.SendCompleted += (o, args) => Logger.Info("Message " + message.Body);
            Logger.Info("Prepare to send mail");
            await smtp.SendMailAsync(message);
            Logger.Info("Mail has been sended");
        }
    }
}